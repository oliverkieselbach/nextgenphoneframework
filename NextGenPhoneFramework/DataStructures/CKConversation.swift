//
//  Call.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 10.09.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore


public enum CKConversationType: Int {
    case inbound = 0
    case outbound = 1
}

public enum CKMessageType: Int {
    case missedCall = 0
    case call = 1
    case video = 2
    case voice = 3
    case text = 4
}

public enum CKMessageMediaType: String {
    case video = "video/mp4"
    case audio = "audio/m4a"
    case text = "text"
    case none = "none"
}


public struct CKMessage {
    

    public var conversationId: String = ""
    public var id: String = ""
    public var inReplyId: String = ""
    public var type: CKConversationType!
    public var messageType: CKMessageType!
    public var receiver: String! = ""
    public var sender: String! = ""
    public var date: Date! = Date()
    public var text: String? = ""
    public var mediaRef: String? = ""
    public var isRead: Bool! = false
    public var topic: String! = ""
    public var mediaURL: String? = ""
    public var mediaType: CKMessageMediaType!
    public var duration: TimeInterval! = 0
    public var senderDetails: CKContactBasic!
    public var receiverDetails: CKContactBasic!
    public var topicDetails: CKIntent!
    public var topicAction: String!
    public var topicActionDetails: CKIntent!


    public var dictionary: [String: Any] {
        return [
            
            "receiver": receiver,
            "sender": sender,
            "inReplyId": inReplyId,
            "type": type.rawValue,
            "messageType": messageType.rawValue,
            "date": date.toFirebaseTimestamp(),
            "text": text == nil ? "" : text,
            "mediaRef": mediaRef == nil ? "" : mediaRef,
            "isRead": isRead,
            "topic": topic == nil ? "" : topic,
            "mediaURL": mediaURL == nil ? "" : mediaURL,
            "mediaType": mediaType == nil ? CKMessageMediaType.none.rawValue : mediaType.rawValue,
            "duration": duration,
            "senderDetails": senderDetails.dictionary,
            "receiverDetails": receiverDetails.dictionary,
            "topicDetails": topicDetails == nil ? [:] : topicDetails.dictionary,
            "topicAction": topicAction == nil ? "" : topicAction,
            "topicActionDetails": topicActionDetails == nil ? [:] : topicActionDetails.dictionary
        ]
        
    }
    
}

extension CKMessage {

    public func write(completion:@escaping (Bool) -> Void) {

        ConversationKit.shared.db.collection(self.sender).document("data").collection("conversations").document(self.conversationId).getDocument { (documentSnapshot, error) in
            
            var conversation: CKConversation!
            if(documentSnapshot?.exists == false) {
                conversation = CKConversation(id: self.conversationId, contact: self.receiver, contactDetails: self.receiverDetails, lastMessageId: self.id, lastMessage: self.date, messagesUnread: 0, lastMessageContent: nil, lastMessageRead: false, intentMessageContent: self)
                
            }
            else {
                conversation = CKConversation(id: self.conversationId, dictionary: (documentSnapshot?.data())!)
                conversation.lastMessage = self.date
                conversation.lastMessageId = self.id
                conversation.lastMessageRead = false
                conversation.lastMessageContent = self
                
            }
            
            
            ConversationKit.shared.db.collection(self.sender).document("data").collection("conversations").document(self.conversationId).collection("messages").document(self.id).setData(self.dictionary, completion: { (error) in
                if(error != nil) {
                    print(error?.localizedDescription)
                    completion(false)
                }
                else {
                    ConversationKit.shared.db.collection(self.sender).document("data").collection("conversations").document(self.conversationId).setData(conversation.dictionary, merge: true, completion: { (error) in
                        if(error != nil) {
                            print(error?.localizedDescription)
                            completion(false)
                            
                        }
                        else {
                            
                            completion(true)
                            
                        }
                    })
                    
                }
            })
        }

    }
    
    public func send(completion:@escaping (Bool) -> Void) {

        
        self.write { (success) in
            if(success) {
                
               
                ConversationKit.shared.db.collection("conversations").document(self.sender).collection((self.conversationId)).addDocument(data: self.dictionary, completion: { (error) in
                    if(error != nil) {
                        print(error?.localizedDescription)
                        completion(false)
                    }
                    else {
                        completion(true)
                    }
                })
                
            }
        }
        
        
        
    }
    
}


extension CKMessage: DocumentSerializable {
    
    public init(conversationId: String, id: String, inReplyId: String, type: CKConversationType, messageType: CKMessageType, receiver: String, sender: String, date: Date, text: String?, mediaRef: String?, isRead: Bool, topic: String, mediaURL: String, mediaType: CKMessageMediaType, duration: TimeInterval, senderDetails: CKContactBasic, receiverDetails: CKContactBasic, topicDetails: CKIntent!, topicAction: String, topicActionDetails: CKIntent!) {
        
        self.id = id
        self.conversationId = conversationId
        self.inReplyId = inReplyId
        self.type = type
        self.messageType = messageType
        self.receiver = receiver
        self.sender = sender
        self.date = date
        self.text = text
        self.mediaRef = mediaRef
        self.isRead = isRead
        self.topic = topic
        self.mediaURL = mediaURL
        self.mediaType = mediaType
        self.duration = duration
        self.senderDetails = senderDetails
        self.receiverDetails = receiverDetails
        self.topicDetails = topicDetails
        self.topicAction = topicAction
        self.topicActionDetails = topicActionDetails
        
    }
    
    public init?(conversationId: String, id: String, dictionary: [String: Any]) {
        self.init(id: id, dictionary: dictionary)
        self.conversationId = conversationId
    }
    
    public init?(id: String, dictionary: [String: Any]) {
        
        let type = CKConversationType(rawValue: (dictionary["type"] as? Int)!)
        let receiver = dictionary["receiver"] as! String
        let sender = dictionary["sender"] as! String
        let inReplyId = dictionary["inReplyId"] as! String
        let messageType = CKMessageType(rawValue: dictionary["messageType"] as! Int)
        let text = dictionary["text"] as! String
        let mediaRef = dictionary["mediaRef"] as! String
        let mediaType = dictionary["mediaType"] == nil ? CKMessageMediaType.none : CKMessageMediaType(rawValue: dictionary["mediaType"] as! String)

        let dateTS = dictionary["date"] as! Timestamp
        let date = Date(timestamp: dateTS)

        let isRead = dictionary["isRead"] == nil ? false : dictionary["isRead"] as! Bool
        let intent = dictionary["topic"] == nil ? "" : dictionary["topic"] as! String
        let mediaURL = dictionary["mediaURL"] == nil ? "" : dictionary["mediaURL"] as! String
        let duration = dictionary["duration"] == nil ? 0 : dictionary["duration"] as! TimeInterval
        
        var senderDetails: CKContactBasic!
        if(dictionary["senderDetails"] != nil) {
            let embeddedContact = dictionary["senderDetails"] as! [String: Any?]
            senderDetails = CKContactBasic(id: sender, dictionary: embeddedContact)
        }
        else {
            senderDetails = nil
        }

        var receiverDetails: CKContactBasic!
        if(dictionary["receiverDetails"] != nil) {
            let embeddedContact = dictionary["receiverDetails"] as! [String: Any?]
            receiverDetails = CKContactBasic(id: sender, dictionary: embeddedContact)
        }
        else {
            receiverDetails = nil
        }

        var intentDetails: CKIntent!
        if(dictionary["topicDetails"] != nil) {
            intentDetails = CKIntent(dictionary: dictionary["topicDetails"] as! [String: Any?])
        }
        else {
            intentDetails = nil
        }
        
        let topicAction = dictionary["topicAction"] == nil ? "" : dictionary["topicAction"] as! String
        var topicActionDetails: CKIntent!
        if(dictionary["topicActionDetails"] != nil) {
            topicActionDetails = CKIntent(dictionary: dictionary["topicActionDetails"] as! [String: Any?])
        }
        else {
            topicActionDetails = nil
        }


        
        self.init(conversationId: "", id: id, inReplyId: inReplyId, type: type, messageType: messageType, receiver: receiver, sender: sender, date: date, text: text, mediaRef: mediaRef, isRead: isRead, topic: intent, mediaURL: mediaURL, mediaType: mediaType, duration: duration, senderDetails: senderDetails, receiverDetails: receiverDetails, topicDetails: intentDetails, topicAction: topicAction, topicActionDetails: topicActionDetails)
    }

}

public struct CKConversation {
    
    public var id: String!
    public var contact: String!
    public var contactDetails: CKContactBasic!
    public var lastMessageId: String!
    public var lastMessage: Date!
    public var messagesUnread: Int!
    public var lastMessageContent: CKMessage!
    public var lastMessageRead: Bool! = false
    public var intentMessageContent: CKMessage!

    public var dictionary: [String: Any] {
        return [
            "contact": contact,
            "contactDetails": contactDetails.dictionary,
            "lastMessageId": lastMessageId,
            "lastMessage": lastMessage.toFirebaseTimestamp(),
            "messagesUnread": messagesUnread,
            "lastMessageRead": lastMessageRead,
            "lastMessageContent": lastMessageContent == nil ? [:] : lastMessageContent.dictionary,
            "intentMessageContent": intentMessageContent.dictionary
        ]
        
    }
}

extension CKConversation: DocumentSerializable {
    
    public static func new(contact: CKContact) -> CKConversation {
        var conversation = CKConversation()
        conversation.id = UUID().uuidString
        conversation.contact = contact.id
        conversation.contactDetails = CKContactBasic(person: contact)
        
        return conversation
    }
    
    public init?(id: String, dictionary: [String: Any]) {

        let lastMessageId = dictionary["lastMessageId"] == nil ? "" : dictionary["lastMessageId"] as! String
        let lastMessage = dictionary["lastMessage"] == nil ? Date() : Date(timestamp: dictionary["lastMessage"] as! Timestamp)
        let messagesUnread = dictionary["messagesUnread"] == nil ? 0 : dictionary["messagesUnread"] as! Int
        let lastMessageRead = dictionary["lastMessageRead"] == nil ? true : dictionary["lastMessageRead"] as! Bool

        let contact = dictionary["contact"] == nil ? "" : dictionary["contact"] as! String
        var contactDetails: CKContactBasic!
        if(dictionary["contactDetails"] != nil) {
            let embeddedContact = dictionary["contactDetails"] as! [String: Any?]
            contactDetails = CKContactBasic(id: contact, dictionary: embeddedContact)
        }
        else {
            contactDetails = nil
        }

        
        var lastMessageContent: CKMessage?
        if(dictionary["lastMessageContent"] != nil) {
            let messageContent = dictionary["lastMessageContent"] as! [String: Any?]
            if(messageContent.values.count != 0) {
                lastMessageContent = CKMessage(conversationId: id, id: lastMessageId, dictionary: messageContent)
            }
            else {
                lastMessageContent = nil
            }
        }
        else {
            lastMessageContent = nil
        }

        var intentMessageContent: CKMessage?
        if(dictionary["intentMessageContent"] != nil) {
            let messageContent = dictionary["intentMessageContent"] as! [String: Any?]
            intentMessageContent = CKMessage(conversationId: id, id: lastMessageId, dictionary: messageContent)
        }
        else {
            let contact = ConversationKit.shared.appUser!
            
            intentMessageContent = CKMessage(conversationId: id, id: "123", inReplyId: "123", type: .inbound, messageType: .text, receiver: contact.id, sender: contact.id, date: Date(), text: "Test", mediaRef: nil, isRead: false, topic: "", mediaURL: nil, mediaType: nil, duration: 0, senderDetails: CKContactBasic(person: contact), receiverDetails: CKContactBasic(person: contact), topicDetails: nil, topicAction: "", topicActionDetails: nil)
        }

        self.init(id: id, contact: contact, contactDetails: contactDetails, lastMessageId: lastMessageId, lastMessage: lastMessage, messagesUnread: messagesUnread, lastMessageContent: lastMessageContent, lastMessageRead: lastMessageRead, intentMessageContent: intentMessageContent)
        
    }

}

public struct Call {
    
    public enum CallStatus: Int {
        case inactive = 0
        case calling = 1
        case rejected = 2
        case active = 3
        case ended = 4
    }
    
    public enum CallType: Int {
        case audio = 0
        case video = 1
    }
    
    public var id: String = ""
    public var callType: CallType!
    public var caller: String!
    public var callerDetails: CKContactBasic!
    public var callee: String!
    public var calleeDetails: CKContactBasic!
    public var started: Date
    public var ended: Date
    public var conversationId: String
    public var status: CallStatus!
    public var intent: CKIntent!
    public var room: String!
    public var contextMessageId: String
    
    public var dictionary: [String: Any] {
        return [
            
            "callType": callType.rawValue,
            "caller": caller,
            "callerDetails": callerDetails.dictionary,
            "callee": callee,
            "calleeDetails": calleeDetails.dictionary,
            "status": status.rawValue,
            "started": started.toFirebaseTimestamp(),
            "ended": ended.toFirebaseTimestamp(),
            "conversationId": conversationId,
            "intent": intent.dictionary,
            "room": room,
            "contextMessageId": contextMessageId
        ]
        
    }
    
    public func type() -> CKConversationType {
        return self.caller == ConversationKit.shared.currentUser.uid ? .outbound : .inbound
    }
    
}

extension Call: DocumentSerializable {
    
    public init?(callee: CKContact, caller: CKContact, conversationId: String) {
        self.init(id: UUID().uuidString, callType: .audio, caller: caller.id, callerDetails: CKContactBasic(person: caller), callee: callee.id, calleeDetails: CKContactBasic(person: callee), started: Date(), ended: Date(), conversationId: conversationId, status: .inactive, intent: CKIntent(dictionary: [:]), room: "", contextMessageId: "")
    }

    public init?(callee: CKContactBasic, caller: CKContactBasic, conversationId: String) {
        self.init(id: UUID().uuidString, callType: .audio, caller: caller.id, callerDetails: caller, callee: callee.id, calleeDetails: callee, started: Date(), ended: Date(), conversationId: conversationId, status: .inactive, intent: CKIntent(dictionary: [:]), room: "", contextMessageId: "")
    }

    
    public init?(id: String, dictionary: [String: Any]) {
        
        let caller = dictionary["caller"] as! String
        let callee = dictionary["callee"] as! String

        var callerDetails: CKContactBasic!
        if(dictionary["callerDetails"] != nil) {
            let embeddedContact = dictionary["callerDetails"] as! [String: Any?]
            callerDetails = CKContactBasic(id: caller, dictionary: embeddedContact)
        }
        else {
            callerDetails = nil
        }
        
        var calleeDetails: CKContactBasic!
        if(dictionary["calleeDetails"] != nil) {
            let embeddedContact = dictionary["calleeDetails"] as! [String: Any?]
            calleeDetails = CKContactBasic(id: callee, dictionary: embeddedContact)
        }
        else {
            calleeDetails = nil
        }

        let callType = dictionary["callType"] == nil ? CallType.audio : CallType(rawValue: dictionary["callType"] as! Int)

        let startedTS = dictionary["started"] as! Timestamp
        let started = Date(timestamp: startedTS)

        let endedTS = dictionary["ended"] as! Timestamp
        let ended = Date(timestamp: endedTS)

        let conversationId = dictionary["conversationId"] == nil ? "" : dictionary["conversationId"] as! String
        let status = dictionary["status"] == nil ? CallStatus.inactive : CallStatus(rawValue: dictionary["status"] as! Int)

        let intent = dictionary["intent"] == nil ? CKIntent(dictionary: [:]) : CKIntent(dictionary: dictionary["intent"] as! [String: Any])
        let room = dictionary["room"] == nil ? "" : dictionary["room"] as! String
        let contextMessageId = dictionary["contextMessageId"] == nil ? "" : dictionary["contextMessageId"] as! String

        
        self.init(id: id, callType: callType, caller: caller, callerDetails: callerDetails, callee: callee, calleeDetails: calleeDetails, started: started!, ended: ended!, conversationId: conversationId, status: status, intent: intent, room: room, contextMessageId: contextMessageId)
        
    }
    
}

public struct CallEntry {
    
    public enum CallEntryType: Int {
        case unknown
        case joinedRoom
        case leftRoom
        case rejectedInvitation
        case image
        case appointment
        case location
        case liveLocation
        
    }
    
    public var sender: String!
    public var date: Date!
    public var type: CallEntryType!
    public var content: Dictionary<String, Any>?
    
    public var dictionary: [String: Any] {
        return [
            "sender": sender,
            "type": type.rawValue,
            "date": date.toFirebaseTimestamp(),
            "content": content
        ]
        
    }
    
    public static func create(type: CallEntryType, sender: String) -> CallEntry {
        
        var entry = CallEntry()
        entry.type = type
        entry.date = Date()
        entry.sender = sender
        entry.content = [:]
        
        return entry
    }
    
}

extension CallEntry: DocumentSerializable {

    public init?(id: String, dictionary: [String: Any]) {

        let sender = dictionary["sender"] == nil ? "" : dictionary["sender"] as! String
        let dateTS = dictionary["date"] as! Timestamp
        let date = Date(timestamp: dateTS)
        let type = dictionary["type"] == nil ? CallEntryType.unknown : CallEntryType(rawValue: dictionary["type"] as! Int)
        let content = dictionary["content"] as! Dictionary<String, Any>
        
        self.init(sender: sender, date: date, type: type, content: content)
        
    }

    
}


public struct IncomingCall {
    
    public var id: String = ""
    public var caller: String!
    public var callerDetails: CKContactBasic!
    public var callee: String!
    public var calleeDetails: CKContactBasic!
    public var status: String!
    public var conversationId: String!
    
    public var dictionary: [String: Any] {
        return [
            
            "caller": caller,
            "callerDetails": callerDetails.dictionary,
            "callee": callee,
            "calleeDetails": calleeDetails.dictionary,
            "status": status
        ]
        
    }

}

extension IncomingCall: DocumentSerializable {
    
    public init?(id: String, dictionary: [String: Any]) {
        
        let caller = dictionary["caller"] as! String
        let callee = dictionary["callee"] as! String
        let status = dictionary["status"] as! String

        var callerDetails: CKContactBasic!
        if(dictionary["callerDetails"] != nil) {
            let embeddedContact = dictionary["callerDetails"] as! [String: Any?]
            callerDetails = CKContactBasic(id: caller, dictionary: embeddedContact)
        }
        else {
            callerDetails = nil
        }

        var calleeDetails: CKContactBasic!
        if(dictionary["calleeDetails"] != nil) {
            let embeddedContact = dictionary["calleeDetails"] as! [String: Any?]
            calleeDetails = CKContactBasic(id: callee, dictionary: embeddedContact)
        }
        else {
            calleeDetails = nil
        }
        
        let conversationId = dictionary["conversationId"] == nil ? "" : dictionary["conversationId"] as! String

        self.init(id: id, caller: caller, callerDetails: callerDetails, callee: callee, calleeDetails: calleeDetails, status: status, conversationId: conversationId)
        
    }
    
}

