//
//  Contact.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 27.08.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import UIKit
import FirebaseFirestore
import FirebaseFunctions

public enum CKContactStatus: Int {
    case unknown = -1
    case available = 0
    case notAvailable = 1
}

protocol DocumentSerializable {
    
    init?(id: String, dictionary: [String:Any])
    
}

public struct CKContactBasic {
    public var id: String
    public var name: String
    public var profileImageRef: String

    public var dictionary: [String:Any] {
        return [
            "name": name,
            "profileImageRef": profileImageRef
        ]
        
    }
}

extension CKContactBasic {
    public func getShareingURL() -> URL {
        
        return URL(string: "nextgenphoneapp:addContact?id="+self.id)!
        
    }
    
    public func getOpenURL() -> URL {
        return URL(string: "nextgenphoneapp:openContact?id="+self.id)!
    }

}
extension CKContactBasic: DocumentSerializable {

    public init?(person: CKContact) {
        
        self.id = person.id
        self.name = person.name
        self.profileImageRef = person.profileImageRef
        
    }
    
    public init?(id: String, dictionary: [String: Any]) {

        let name = dictionary["name"] == nil ? "unknown" : dictionary["name"] as! String
        let profileImageRef = dictionary["profileImageRef"] == nil ? "" : dictionary["profileImageRef"] as! String

        self.init(id: id, name: name, profileImageRef: profileImageRef)
    }
}


public struct CKContact {
    
    public var id: String = ""
    public var name: String
    public var phoneNumber: String
    public var description: String
    public var status: CKContactStatus!
    public var statusUpdated: Date
    public var profileImageRef: String
    public var numberOfCalls: Int
    public var statusContent: CKContactStatusContent?
    public var blocked: Bool = false
    public var vip: Bool = false
    
    public var dictionary: [String: Any] {
        
        var dict: [String: Any] = [
            
            "name": name,
            "phoneNumber": phoneNumber,
            "description": description,
            "status": status.rawValue,
            "statusUpdated": statusUpdated.toFirebaseTimestamp(),
            "profileImageRef": profileImageRef,
            "numberOfCalls": numberOfCalls,
            "blocked": blocked,
            "vip": vip,
            
            ]
        
        if(statusContent != nil) {
            dict = dict.merging((statusContent?.dictionary)!) { (current, _) in current}
        }
        
        return dict
            
    }
    
}

extension CKContact {
    
    static public func delete(contact: CKContact, completion: @escaping ((Bool) -> Void)) {
        
        ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").document(contact.id).delete { (error) in
            if(error != nil) {
                print(error?.localizedDescription)
                completion(false)
            }
            else {
                completion(true)
            }
        }
        
    }
    
    static public func saveInMyContactList(contact: CKContact) {
        
        ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").document(contact.id).setData(contact.dictionary, merge: true) { (error) in
            if(error != nil) {
                print(error?.localizedDescription)
            }
        }
        
    }
    
    static public func addToMyContactList(contact: CKContact, completion: @escaping ((Bool) -> Void)) {
        var contactDict = contact.dictionary
        contactDict["lastContact"] = Timestamp(date: Date())
        ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").document(contact.id).setData(contactDict) { (error) in
            if(error != nil) {
                print(error?.localizedDescription)
                completion(false)
            }
            else {
                let functions = Functions.functions()
                functions.httpsCallable("addFollower").call(["targetUser": contact.id, "follower": ConversationKit.shared.currentUser.uid]) { (result, error) in
                    
                    if let error = error as NSError? {
                        if error.domain == FunctionsErrorDomain {
                            let code = FunctionsErrorCode(rawValue: error.code)
                            let message = error.localizedDescription
                            let details = error.userInfo[FunctionsErrorDetailsKey]
                            print(message)
                            completion(false)
                        }
                    }
                    else {
                        completion(true)
                    }

                }
            }
            
        }
        
        
    }
    
    public func getShareingURL() -> URL {
        
        return URL(string: "nextgenphoneapp:addContact?id="+self.id)!
        
    }
    
    public func getOpenURL() -> URL {
        return URL(string: "nextgenphoneapp:openContact?id="+self.id)!
    }
    
    public func getPersonalIntents() -> [CKAction] {
        
        let action1 = CKAction(name: "Bitte um Rückruf", description: "Bitte um Rückruf", type: .action, functionName: "callmebackAction", intentQuery: "Bitte um Rückruf")
        
        let action2 = CKAction(name: "Termin Anfrage", description: "Bitte um Termin", type: .action, functionName: "askforappointmentAction", intentQuery: "Bitte um einen Termin")

        return [action1, action2]
    }

}

extension CKContact: DocumentSerializable {
    
    public init(id: String, name: String, phoneNumber: String, description: String, status: CKContactStatus, statusUpdated: Date, profileImageRef: String, numberOfCalls: Int, statusContent: CKContactStatusContent!, blocked: Bool, vip: Bool) {
        
        self.id = id
        self.name = name
        self.phoneNumber = phoneNumber
        self.description = description
        self.status = status
        self.statusUpdated = statusUpdated
        self.profileImageRef = profileImageRef
        self.numberOfCalls = numberOfCalls
        self.statusContent = statusContent
        self.blocked = blocked
        self.vip = vip
        
        
    }
    
    public init?(personEmbedded: CKContactBasic) {
        self.init(id: personEmbedded.id, name: personEmbedded.name, phoneNumber: "", description: "", status: .unknown, statusUpdated: Date(), profileImageRef: personEmbedded.profileImageRef, numberOfCalls: 0, statusContent: nil, blocked: false, vip: false)
    }
    
    public init?(id: String, dictionary: [String: Any]) {
        var timestamp: Timestamp
        if(dictionary["statusUpdated"] == nil) {
            timestamp = Timestamp(date: Date())
        }
        else {
            timestamp = dictionary["statusUpdated"] as! Timestamp
        }
        
        let statusUpdated = Date(timestamp: timestamp)
        let status = CKContactStatus(rawValue: (dictionary["status"] as? Int)!)
        let numberOfCalls:Int = dictionary["numberOfCalls"] == nil ? 0 : dictionary["numberOfCalls"] as! Int
        let backgroundImageRef = dictionary["backgroundImageRef"] == nil ? "" : dictionary["backgroundImageRef"] as? String
        let statusContent = dictionary["statusContent.contentURL"] == nil ? nil : CKContactStatusContent(id: id, dictionary: dictionary)
        let blocked = dictionary["blocked"] == nil ? false : dictionary["blocked"] as! Bool
        let vip = dictionary["vip"] == nil ? false : dictionary["vip"] as! Bool
        let description = dictionary["description"] == nil ? "This is a description text of the contact, like Position etc" : dictionary["description"] as! String

        
        guard let name = dictionary["name"] as? String,
            let phoneNumber = dictionary["phoneNumber"] as? String,
            let profileImageRef = dictionary["profileImageRef"] as? String
             else {
                return nil
        }
        
        self.init(id: id, name: name, phoneNumber: phoneNumber, description: description, status: status!, statusUpdated: statusUpdated!, profileImageRef: profileImageRef, numberOfCalls: numberOfCalls, statusContent: statusContent, blocked: blocked, vip: vip)
        
    }
    
}

public enum CKContactStatusContentType: String {
    case audio = "audio/m4a"
    case video = "video/mp4"
}

public struct CKContactStatusContent {
    
    public var id: String = ""
    public var contentURL: String
    public var transcript: String?
    public var duration: TimeInterval
    public var recorded: Date
    public var contentType: CKContactStatusContentType?
    
    public var dictionary: [String: Any] {
        return [
            "absenceMessage.contentURL": contentURL,
            "absenceMessage.transcript" : transcript == nil ? "" : transcript as? String,
            "absenceMessage.duration": duration,
            "absenceMessage.recorded": recorded,
            "absenceMessage.contentType": contentType!.rawValue
        ]
        
    }
    
}

extension CKContactStatusContent: DocumentSerializable {
    
    public init(id: String, contentURL: String, transcript: String?, duration: TimeInterval, recorded: Date, contentType: CKContactStatusContentType) {
        
        self.id = id
        self.contentURL = contentURL
        self.transcript = transcript
        self.duration = duration
        self.recorded = recorded
        self.contentType = contentType
        
    }
    
    public init?(id: String, dictionary: [String: Any]) {
        let timestamp = dictionary["statusContent.recorded"] == nil ? nil : dictionary["statusContent.recorded"] as! Timestamp
        var recorded = Date()
        if timestamp != nil {
            recorded = Date(timestamp: timestamp!)!
        }
        
        let contentURL = dictionary["statusContent.contentURL"] == nil ? "" : dictionary["statusContent.contentURL"] as? String
        let transcript = dictionary["statusContent.transcript"] == nil ? "" : dictionary["statusContent.transcript"] as? String
        
        let duration = dictionary["statusContent.duration"] == nil ? 0 : dictionary["statusContent.duration"] as! TimeInterval
        
        let contentType = dictionary["statusContent.contentType"] == nil ? CKContactStatusContentType.audio : CKContactStatusContentType(rawValue: dictionary["statusContent.contentType"] as! String)
        
        self.init(id: id, contentURL: contentURL!, transcript: transcript, duration: duration, recorded: recorded, contentType: contentType!)
        
    }
    
}

