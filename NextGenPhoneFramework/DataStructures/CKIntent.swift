//
//  Intent.swift
//  NextGenPhoneApp
//
//  Created by Kieselbach, Oliver on 29.01.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import FirebaseFirestore

public enum CKActionType: String {
    case action = "action"
    case inApp = "inApp"
}

public struct CKAction {
    public var name: String
    public var description: String
    public var type: CKActionType
    public var functionName: String
    public var intentQuery: String
    
}

/*
public struct CKIntentAction {
    public var id: String
    public var name: String
    public var description: String
    public var formDocumentId: String
    public var actions: [CKAction]
    
    public var dictionary: [String: Any] {
        return [
            "id": id,
            "name": name,
            "description": description,
            "formDocumentId": formDocumentId
        ]
        
    }
    

    
}
 


extension CKIntentAction: DocumentSerializable {
    public init?(dictionary: [String : Any]) {
        let id = dictionary["id"] == nil ? "" : dictionary["id"] as! String
        self.init(id: id, dictionary: dictionary)
    }
    
    public init?(id: String, dictionary: [String : Any]) {
        
        let name = dictionary["name"] == nil ? "" : dictionary["name"] as! String
        let description = dictionary["description"] == nil ? "" : dictionary["description"] as! String
        //let parameter = dictionary["parameter"] == nil ? [] : CKIntent.paramFromDict(dict: dictionary["parameter"] as! [String:Any])
        let formDocumentId = dictionary["formDocumentId"] == nil ? "" : dictionary["formDocumentId"] as! String
        
        var actions: [CKAction] = []
        if(dictionary["actions"] != nil) {
            let actionsData = dictionary["actions"] as! [[String:Any]]
            for action in actionsData {
                
                let value = action as! [String:Any]
                let name = value["name"] == nil ? "" : value["name"] as! String
                let description = value["description"] == nil ? "" : value["description"] as! String
                let functionName = value["functionName"] == nil ? "" : value["functionName"] as! String
                let type = value["type"] == nil ? CKActionType.inApp : CKActionType(rawValue: value["type"] as! String)
                
                let actionDef = CKAction(name: name, description: description, type: type!, functionName: functionName)
                actions.append(actionDef)
            }
        }

        self.init(id: id, name: name, description: description, formDocumentId: formDocumentId, actions: actions)
        
    }
    
    
}
*/

public enum CKIntentColor: String {
    case blue = "blue"
    case yellow = "yellow"
    case red = "red"
    case orange = "orange"
    case gray = "gray"
    case green = "green"
    case cyan = "cyan"
}

public struct CKIntent {
    public var id: String
    public var name: String
    public var description: String
    public var confidenceLevel: Double
    public var formDocumentId: String
    public var actions: [CKAction]
    public var color: CKIntentColor
    
    
    public var dictionary: [String: Any] {
        return [
            "id": id,
            "name": name,
            "description": description,
            "confidenceLevel": confidenceLevel,
            "formDocumentId": formDocumentId,
            "color": color.rawValue
        ]
        
    }

}

public struct CKIntentColors {

    public var tintColor: UIColor
    public var backgroundColor: UIColor

}

extension CKIntent {

    public func getColor() -> CKIntentColors {

        var colors = CKIntentColors(tintColor: .darkGray, backgroundColor: .lightGray)
        
        
        switch self.color {
        case .blue:
            //colors = CKIntentColors(tintColor: UIColor(0x039BE5), backgroundColor: UIColor(0xE1F5FE))
            colors = CKIntentColors(tintColor: UIColor(0x2ABAE4), backgroundColor: UIColor(0xD8F6FF))
        case .red:
            colors = CKIntentColors(tintColor: UIColor(0xF26D6E), backgroundColor: UIColor(0xFFEFEF))
        case .orange:
            //colors = CKIntentColors(tintColor: UIColor(0xFB8C00), backgroundColor: UIColor(0xFFF3E0))
            colors = CKIntentColors(tintColor: UIColor(0xFE9101), backgroundColor: UIColor(0xFFE7C7))
        case .yellow:
            //colors = CKIntentColors(tintColor: UIColor(0xF57F17), backgroundColor: UIColor(0xFFFDE7))
            colors = CKIntentColors(tintColor: UIColor(0xC53CE1), backgroundColor: UIColor(0xFBE9FF))
        case .green:
            //colors = CKIntentColors(tintColor: UIColor(0x7CB342), backgroundColor: UIColor(0xF1F8E9))
            colors = CKIntentColors(tintColor: UIColor(0x63C015), backgroundColor: UIColor(0xE4FFCE))
        case .cyan:
            colors = CKIntentColors(tintColor: UIColor(0x08CCA0), backgroundColor: UIColor(0xCDFFF4))
        case .gray:
            colors = CKIntentColors(tintColor: UIColor(0xB71C1C), backgroundColor: UIColor(0xFFEBEE))
        default:
            colors = CKIntentColors(tintColor: UIColor(0x424242), backgroundColor: UIColor(0xFAFAFA))
        }
        
        return colors


    }

}

extension CKIntent: DocumentSerializable {
    public init?(dictionary: [String : Any]) {
        let id = dictionary["id"] == nil ? "" : dictionary["id"] as! String
        self.init(id: id, dictionary: dictionary)
    }

    public init?(id: String, dictionary: [String : Any]) {
        
        let name = dictionary["name"] == nil ? "" : dictionary["name"] as! String
        let description = dictionary["description"] == nil ? "" : dictionary["description"] as! String
        let confidenceLevel = dictionary["confidenceLevel"] == nil ? 0 : dictionary["confidenceLevel"] as! Double
        let formDocumentId = dictionary["formDocumentId"] == nil ? "" : dictionary["formDocumentId"] as! String
        let color = dictionary["color"] == nil ? CKIntentColor.blue : CKIntentColor(rawValue: dictionary["color"] as! String)

        var actions: [CKAction] = []
        if(dictionary["actions"] != nil) {
            let actionsData = dictionary["actions"] as! [[String:Any]]
            for action in actionsData {
                
                let value = action as! [String:Any]
                let name = value["name"] == nil ? "" : value["name"] as! String
                let description = value["description"] == nil ? "" : value["description"] as! String
                let functionName = value["functionName"] == nil ? "" : value["functionName"] as! String
                let type = value["type"] == nil ? CKActionType.inApp : CKActionType(rawValue: value["type"] as! String)
                let intentQuery = value["intentQuery"] == nil ? "" : value["intentQuery"] as! String

                let actionDef = CKAction(name: name, description: description, type: type!, functionName: functionName, intentQuery: intentQuery)
                actions.append(actionDef)
            }
        }

        self.init(id: id, name: name, description: description, confidenceLevel: confidenceLevel, formDocumentId: formDocumentId, actions: actions, color: color!)
        
    }
    
    
    

}



