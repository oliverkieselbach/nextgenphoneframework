//
//  NextGenPhoneFramework.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 01.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseFirestore
import FirebaseAuth
import FirebaseStorage
import FirebaseUI
import FirebaseDynamicLinks

public enum CKContactOrder: String {
    case name = "name"
    case statusUpdated = "statusUpdated"
}

open class Const: NSObject {

    static let appLinkHost: String = "app.conversationkit"
    static let appLinkAuthenticationPath: String = "/authentication"
    static let authenticationCallbackURL: String = "conversationkit.app.1234://authentication"
    
    static let dynamicLinkPrefix: String = "https://conversationkitapp.page.link"
    static let dynamicLinkAppBundleId: String = "com.kieselbach.NextGenPhoneApp"
    static let dynamicLinkAppStoreId: String = "962194608" //Google Photos Id

}

public class ConversationKit: NSObject {
    
    
    public var clientKey: String!
    public var db: Firestore!
    public var storage: Storage!

    public var currentUser: User!
    public var appUser: CKContact!
    
    public static var shared: ConversationKit!
    public static var auth: ConversationAuth!
    
    
    public static func configure(clientKey: String) -> ConversationKit {
        
        FirebaseConfiguration.shared.setLoggerLevel(.min)
        
        /* Handling multiple plist files
         let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist")!
         let options = FirebaseOptions(contentsOfFile: filePath)
         FirebaseApp.configure(options: options!)
        */
        
        FirebaseApp.configure()
        
        ConversationKit.shared = ConversationKit()
        ConversationKit.shared.clientKey = clientKey
        ConversationKit.shared.db = Firestore.firestore()
        let settings = ConversationKit.shared.db.settings
        settings.areTimestampsInSnapshotsEnabled = true
        settings.isPersistenceEnabled = true
        ConversationKit.shared.db.settings = settings
        
        ConversationKit.shared.storage = Storage.storage()

        ConversationKit.auth = ConversationAuth()
        
        return ConversationKit.shared
        
    }
    
    static public func requestAuthenticationToken() {
        
        let url = ConversationKit.auth.generateAuthenticationDynamicLink()
        
        print(url?.absoluteString ?? "No URL")
        UIApplication.shared.open(url!, options: [:]) { (success) in
        }
        
    }
    

    public func ping() {
        
        
        self.db.collection("ping").document("helloworld").getDocument { (documentSnapshot, error) in
            if(error != nil) {
                print(error?.localizedDescription ?? "Error in Pung")
            }
            else {
                guard let document = documentSnapshot else {
                    return
                }
                let data = document.data()
                print(data)
            }
        }
        
    }

    public func isSignedIn() -> Bool {
        if(self.currentUser == nil || self.currentUser.isAnonymous) {
            return false
        }
        return true
    }
    
    public static func handleURLOpen(url: URL) -> Bool {
        
        let components = URLComponents(url: url, resolvingAgainstBaseURL: true)
        
        if(components == nil) {
            print("Error opening URL")
            return false
        }
        
        let command = components?.host
        switch command {
        case "authentication":
            return ConversationKit.auth.handleAuthenticationRequest(params: components?.queryItems)
        default:
            break
        }
        
        return false
        
    }
    
}

public class ConversationAuth: NSObject {

    var stateChangeListenerHandle: AuthStateDidChangeListenerHandle!
    override init() {
        
        self.stateChangeListenerHandle = Auth.auth().addStateDidChangeListener { (auth, user) in
            print("Authentication State Change Listener")
            
            if(user?.isAnonymous == false) {
                ConversationKit.shared.db.collection("users").document(user!.uid).getDocument { (documentSnapshot, error) in
                    
                    guard let document = documentSnapshot else {
                        print(error?.localizedDescription ?? "Error reading AppUser")
                        return
                    }
                    
                    ConversationKit.shared.appUser = CKContact(id: document.documentID, dictionary: document.data()!)
                    NotificationCenter.default.post(name: .userSignedIn, object: nil)

                }

            }
            else {
                ConversationKit.shared.appUser = nil
                
            }
        }
        
        ConversationKit.shared.currentUser = Auth.auth().currentUser
        if(ConversationKit.shared.currentUser == nil) {

            Auth.auth().signInAnonymously { (authDataResult, error) in
                if(error != nil) {
                    print(error?.localizedDescription ?? "Error in SignIn Anonmymously")
                    return
                }
                print("Successful anonymously authentication")
                ConversationKit.shared.currentUser = authDataResult?.user
                
            }
            
        }
        else {
            print("User is signed in")
            
        }

        
    }
  
    
    deinit {
        if(self.stateChangeListenerHandle != nil) {
            self.stateChangeListenerHandle = nil
        }
    }
    
    public func signOut() {
        
        if(ConversationKit.shared.currentUser.isAnonymous == false) {
            
            do {
                try Auth.auth().signOut()
                Auth.auth().signInAnonymously { (authDataResult, error) in
                    if(error != nil) {
                        print(error?.localizedDescription ?? "Error in SignIn Anonmymously")
                        return
                    }
                    print("Successful anonymously authentication")
                    ConversationKit.shared.currentUser = authDataResult?.user
                    NotificationCenter.default.post(name: .userSignedOut, object: nil)

                }

            }
            catch {
                print("Error")
            }
            
        }
        
    }
    
    public func handleAuthenticationRequest(params: [URLQueryItem]?) -> Bool {
        var token = ""
        if(params != nil) {
            for param in params! {
                if(param.name == "token") {
                    token = param.value == nil ? "" : param.value!
                }
            }
        }
        if(token != "") {
            self.authenticate(token: token)
            return true
        }
        return false
    }
    
    private func authenticate(token: String) {
        
        print("ConversationKit.authenticate "+token)
        

        Auth.auth().signIn(withCustomToken: token) { (authDataResult, error) in

            let controller = DJSemiModalViewController()
            
            if(error == nil) {

                controller.title = "Welcome"
                
                let avatar = CKContactView(frame: .zero)
                avatar.translatesAutoresizingMaskIntoConstraints = false
                avatar.heightAnchor.constraint(equalToConstant: 100).isActive = true
                avatar.widthAnchor.constraint(equalToConstant: 100).isActive = true
                controller.addArrangedSubview(view: avatar)
                
                let nameLabel = UILabel()
                nameLabel.translatesAutoresizingMaskIntoConstraints = false
                nameLabel.widthAnchor.constraint(equalToConstant: (UIApplication.shared.keyWindow?.rootViewController!.view.frame.size.width)! * 0.75).isActive = true
                nameLabel.textAlignment = .center
                nameLabel.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
                nameLabel.textColor = UIColor.black
                controller.addArrangedSubview(view: nameLabel)

                ConversationKit.shared.db.collection("users").document(authDataResult!.user.uid).getDocument { (documentSnapshot, error) in
                    
                    guard let document = documentSnapshot else {
                        print(error?.localizedDescription ?? "Error reading AppUser")
                        return
                    }
                    
                    ConversationKit.shared.appUser = CKContact(id: document.documentID, dictionary: document.data()!)

                    avatar.contact = ConversationKit.shared.appUser
                    nameLabel.text = ConversationKit.shared.appUser.name
                    
                    
                }

            }
            else {
                
                controller.title = "Error"

                
            }


            
            controller.automaticallyAdjustsContentHeight = true
            
            controller.maxWidth = 420
            
            controller.minHeight = 200
            
            controller.titleLabel.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight.bold)
            
            controller.closeButton.setTitle("Done", for: .normal)
            
            var topMostVC = UIApplication.getTopMostViewController()
            if(topMostVC?.navigationController != nil) {
                topMostVC = topMostVC?.navigationController
            }
            
            controller.presentOn(presentingViewController: topMostVC!, animated: true, onDismiss: { })



            if(error != nil) {
                print(error?.localizedDescription ?? "Error authenticating user")
            }
            else {
                
                print("Successful authenticated user")
                ConversationKit.shared.currentUser = authDataResult!.user
                
            }
        }

        
    }
    
    open func generateAuthenticationDynamicLink() -> URL? {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = Const.appLinkHost
        components.path = Const.appLinkAuthenticationPath
        
        let authenticationRequestParams = URLQueryItem(name: "clientKey", value: ConversationKit.shared.clientKey)
        let callbackURL = URLQueryItem(name: "appCallbackURL", value: Const.authenticationCallbackURL)
        
        components.queryItems = [authenticationRequestParams, callbackURL]
        
        guard let dynamicLink = DynamicLinkComponents.init(link: components.url!, domainURIPrefix: Const.dynamicLinkPrefix) else {
            print("Error creating dynamic link for authentication request")
            return nil
        }
        
        dynamicLink.iOSParameters = DynamicLinkIOSParameters(bundleID: Const.dynamicLinkAppBundleId)
        dynamicLink.iOSParameters?.appStoreID = Const.dynamicLinkAppStoreId
        
        dynamicLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters()
        dynamicLink.socialMetaTagParameters?.title = "Request User Authentication"
        dynamicLink.socialMetaTagParameters?.descriptionText = "This is a description text"
        
        guard let longURL = dynamicLink.url else {return nil}
        
        return longURL
        
        
    }
    

    
}
