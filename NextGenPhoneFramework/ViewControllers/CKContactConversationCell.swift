//
//  CKContactConversationCell.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 24.06.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import SwiftRichString

public class ConversationMessageViewLarge: UIView {
    
    public var message: CKMessage! {
        didSet {

            let colors = message.topicActionDetails == nil || message.topicActionDetails.id == "" ? message.topicDetails.getColor() : message.topicActionDetails.getColor()
            self.backgroundColor = colors.backgroundColor
            self.tintColor = colors.tintColor
            
            
            self.attachmentButton.tintColor = colors.tintColor
            self.intentLabel.textColor = colors.tintColor
            let intentLabelText = message.topicActionDetails.id == "" ? message.topicDetails.name : message.topicActionDetails.name
            
            let normal = Style {
                $0.font = UIFont.systemFont(ofSize: 15, weight: .bold)
                $0.color = colors.tintColor
                $0.minimumLineHeight = 20
            }
            
            let senderName = Style {
                $0.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                $0.color = colors.tintColor
                $0.minimumLineHeight = 12

            }
            
            // Create a group which contains your style, each identified by a tag.
            let myGroup = StyleGroup(base: normal, ["senderName": senderName])
            
            // Use tags in your plain string
            let str = "<senderName>\(message.senderDetails.name)</senderName>\n\(intentLabelText)"
            self.intentLabel.attributedText = str.set(style: myGroup)
            self.intentLabel.translatesAutoresizingMaskIntoConstraints = true
            self.intentLabel.sizeToFit()
            self.intentLabel.widthAnchor.constraint(equalToConstant: self.intentLabel.frame.width).isActive = true
            
            self.intentLabel.translatesAutoresizingMaskIntoConstraints = false
        
            

            var imageName = "play"
            switch message.messageType! {
            case .voice:
                imageName = "unmuted_filled"
                stackView.addArrangedSubview(self.attachmentButton)
                self.attachmentButton.isHidden = false
                //stackView.setCustomSpacing(30, after: self.intentLabel)
            case .video:
                imageName = "videocall_filled"
                stackView.addArrangedSubview(self.attachmentButton)
                self.attachmentButton.isHidden = false
                //stackView.setCustomSpacing(30, after: self.intentLabel)
            default:
                imageName = "play"
                stackView.removeArrangedSubview(self.attachmentButton)
                self.attachmentButton.isHidden = true
                stackView.setCustomSpacing(5, after: self.intentLabel)
            }
            
            self.setNeedsUpdateConstraints()
            self.layoutIfNeeded()
            
            
            self.attachmentButton.setImage(UIImage(imageLiteralResourceName: imageName).withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)

            
            self.avatar.contact = CKContact(personEmbedded: message.senderDetails)
            
        }
    }
    
    public var intent: CKIntent! {
        didSet {
            
            let colors = intent.getColor()
            self.backgroundColor = colors.backgroundColor
            self.tintColor = colors.tintColor

            self.intentLabel.text = intent.name
            self.intentLabel.textColor = colors.tintColor
            stackView.removeArrangedSubview(self.avatar)
        }
    }
    
    public var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
        }
    }
    
    var stackView: UIStackView!
    
    var intentLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.textAlignment = .left
        label.numberOfLines = 2
        
        
        return label
    }()
    
    public var attachmentButton: UIButton = {
        
        let button = UIButton(type: .custom)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 25).isActive = true
        button.widthAnchor.constraint(equalToConstant: 25).isActive = true
        
        button.backgroundColor = .clear
        
        button.tintColor = UIView().tintColor
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        
        return button
        
        
    }()


    
    var avatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        return v
        
    }()
    

    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 5

        
        stackView.addArrangedSubview(self.avatar)
        stackView.addArrangedSubview(self.intentLabel)
        
        self.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        self.widthAnchor.constraint(equalTo: stackView.widthAnchor, constant: 50).isActive = true

        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        
        super.layoutSubviews()
        //self.setShadowWithColor(color: UIColor.gray, opacity: 0.3, offset: CGSize(width: 1, height: 1), radius: 3, viewCornerRadius: 25)
        self.layer.cornerRadius = self.frame.height / 2
        
    
        
    }
    
}

class ConversationMessageViewSmall: UIView {
    
    var message: CKMessage! {
        didSet {
            
            let colors = message.topicDetails.getColor()
            self.backgroundColor = colors.backgroundColor
            self.tintColor = colors.tintColor
            
            self.intentLabel.textColor = colors.tintColor
            self.intentLabel.text = message.topicDetails.name
            
            self.avatar.contact = CKContact(personEmbedded: message.senderDetails)

            
            
        }
    }

    var avatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 35).isActive = true
        v.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        return v
        
    }()

    var intentLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        label.textAlignment = .center
        
        return label
    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 5
        
        stackView.addArrangedSubview(self.avatar)
        stackView.addArrangedSubview(self.intentLabel)

        self.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        self.widthAnchor.constraint(equalTo: stackView.widthAnchor, constant: 25).isActive = true
/*
        self.addSubview(self.avatar)
        self.addSubview(self.intentLabel)
        
        self.avatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: -5).isActive = true
        self.avatar.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true

        self.intentLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.intentLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
*/
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    
}

public class CKContactConversationBaseCell: UITableViewCell {

    public var conversation: CKConversation!
    
    public var playMessageAction: ((CKMessage) -> Void)?
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.accessoryType = .disclosureIndicator

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    



}

public class CKContactConversationFirstMessageOutCell: CKContactConversationBaseCell {
    static public var reuseId = "ConversationFirstMessageOutCell"
    
    public override var conversation: CKConversation! {
        didSet {
            
            self.intentMessage = conversation.intentMessageContent
            
        }
    }
    
    var intentMessage: CKMessage! {
        didSet {
            
            self.messageView.message = intentMessage
            self.dateLabel.text = intentMessage.date.timeAgoSinceNow
            self.recipientAvatar.contact = CKContact(personEmbedded: intentMessage.receiverDetails)

            
        }
    }
    
    var messageView: ConversationMessageViewLarge = {

        var v = ConversationMessageViewLarge(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false

        v.heightAnchor.constraint(equalToConstant: 55).isActive = true
        v.layer.cornerRadius = 55/3
        
        return v
        
    }()
    
    var recipientAvatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 45).isActive = true
        v.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        return v
        
    }()

    var dateLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textAlignment = .right
        
        return label
    }()



    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let dashedView = DashedLineView(frame: .zero)
        dashedView.translatesAutoresizingMaskIntoConstraints = false

        self.contentView.addSubview(self.messageView)
        self.contentView.addSubview(self.dateLabel)
        self.contentView.addSubview(dashedView)
        self.contentView.addSubview(self.recipientAvatar)
        
        self.messageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.messageView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        /*
        self.messageView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.messageView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        */
 
        dashedView.topAnchor.constraint(equalTo: self.messageView.bottomAnchor, constant: 5).isActive = true
        dashedView.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        dashedView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        dashedView.heightAnchor.constraint(equalToConstant: 10).isActive = true

        self.recipientAvatar.topAnchor.constraint(equalTo: dashedView.bottomAnchor, constant: 5).isActive = true
        self.recipientAvatar.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.recipientAvatar.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -15).isActive = true

        
        self.dateLabel.topAnchor.constraint(equalTo: self.messageView.bottomAnchor, constant: 3).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.messageView.rightAnchor, constant: 0).isActive = true
        
        self.messageView.attachmentButton.addTarget(self, action: #selector(self.playAttachmentAction), for: .touchDown)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func playAttachmentAction() {
        if(self.playMessageAction != nil) {
            self.playMessageAction!(self.intentMessage)
        }
    }

    
}

public class CKContactConversationFirstMessageInCell: CKContactConversationBaseCell {
    static public var reuseId = "ConversationFirstMessageInCell"

    public override var conversation: CKConversation! {
        didSet {
            
            self.intentMessage = conversation.intentMessageContent
            
        }
    }
    
    var intentMessage: CKMessage! {
        didSet {
            
            self.messageView.message = intentMessage
            self.dateLabel.text = intentMessage.date.timeAgoSinceNow
            self.recipientAvatar.contact = CKContact(personEmbedded: intentMessage.receiverDetails)
            
            self.badge = BadgeHub(view: self.messageView)
            self.badge.increment()
            self.badge.hideCount()
            self.badge.setCircleColor(UIView().tintColor, label: .white)
            self.badge.setCircleAtFrame(CGRect(origin: CGPoint(x: -20, y: self.center.y), size: CGSize(width: 10, height: 10)))
            
            
        }
    }
    
    var messageView: ConversationMessageViewLarge = {
        
        var v = ConversationMessageViewLarge(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.heightAnchor.constraint(equalToConstant: 55).isActive = true
        v.layer.cornerRadius = 55/3
        
        return v
        
    }()
    
    var recipientAvatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 45).isActive = true
        v.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        return v
        
    }()
    
    var dateLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textAlignment = .right
        
        return label
    }()
    
    var badge: BadgeHub!
    
    
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        self.contentView.addSubview(self.messageView)
        self.contentView.addSubview(self.dateLabel)
        
        self.messageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.messageView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.dateLabel.topAnchor.constraint(equalTo: self.messageView.bottomAnchor, constant: 3).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.messageView.rightAnchor, constant: 0).isActive = true
        self.dateLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -15).isActive = true

        
        self.messageView.attachmentButton.addTarget(self, action: #selector(self.playAttachmentAction), for: .touchDown)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func playAttachmentAction() {
        if(self.playMessageAction != nil) {
            self.playMessageAction!(self.intentMessage)
        }
    }

    

}


public class CKContactConversationCell: CKContactConversationBaseCell {
    static public var reuseId = "ConversationCell"

    public override var conversation: CKConversation! {
        didSet {
            
            self.intentMessage = conversation.intentMessageContent
            self.lastMessage = conversation.lastMessageContent
            
            
            
        }
    }
    
    var intentMessage: CKMessage! {
        didSet {
            self.firstMessageView.message = intentMessage
        }
    }

    var lastMessage: CKMessage! {
        didSet {
            self.lastMessageView.message = lastMessage
            self.dateLabel.text = lastMessage.date.timeAgoSinceNow

        }
    }
    
    var firstMessageView: ConversationMessageViewSmall = {
        
        var v = ConversationMessageViewSmall(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.heightAnchor.constraint(equalToConstant: 35).isActive = true
        v.layer.cornerRadius = 35/2
        return v
        
    }()

    var lastMessageView: ConversationMessageViewLarge = {
        
        var v = ConversationMessageViewLarge(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        v.heightAnchor.constraint(equalToConstant: 55).isActive = true
        v.layer.cornerRadius = 55/3
        
        return v
        
    }()

    var dateLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textAlignment = .right
        
        return label
    }()
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        let dashedView = DashedLineView(frame: .zero)
        dashedView.translatesAutoresizingMaskIntoConstraints = false

        
        self.contentView.addSubview(self.firstMessageView)
        self.contentView.addSubview(self.lastMessageView)
        self.contentView.addSubview(self.dateLabel)
        self.contentView.addSubview(dashedView)

        
        self.firstMessageView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 15).isActive = true
        self.firstMessageView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        dashedView.topAnchor.constraint(equalTo: self.firstMessageView.bottomAnchor, constant: 5).isActive = true
        dashedView.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        dashedView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        dashedView.heightAnchor.constraint(equalToConstant: 10).isActive = true

        
        self.lastMessageView.topAnchor.constraint(equalTo: dashedView.bottomAnchor, constant: 5).isActive = true
        self.lastMessageView.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        
        self.dateLabel.topAnchor.constraint(equalTo: self.lastMessageView.bottomAnchor, constant: 3).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.lastMessageView.rightAnchor, constant: 0).isActive = true
        self.dateLabel.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -15).isActive = true
        
        self.lastMessageView.attachmentButton.addTarget(self, action: #selector(self.playAttachmentAction), for: .touchDown)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func playAttachmentAction() {
        if(self.playMessageAction != nil) {
            self.playMessageAction!(self.lastMessage)
        }
    }




}
