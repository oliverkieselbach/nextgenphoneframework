//
//  CKContactCollectionViewController.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 11.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseFirestore
import FirebaseUI

private let reuseIdentifier = "ckContactCollectionCell"

class CKContactCollectionCell: UICollectionViewCell {
    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
            self.contactName.text = contact.name
        }
    }
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 60).isActive = true //100
        a.widthAnchor.constraint(equalToConstant: 60).isActive = true //100
        
        return a
    }()
    
    var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular)
        label.textAlignment = .center
        label.numberOfLines = 2
        
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.contentView.addSubview(self.avatar)
        self.contentView.addSubview(self.contactName)
        //self.avatar.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 3).isActive = true
        self.avatar.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: -10).isActive = true
        self.avatar.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor, constant: 0).isActive = true
        
        self.contactName.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 3).isActive = true
        self.contactName.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 0).isActive = true
        self.contactName.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 0).isActive = true
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


public class CKContactCollectionViewController: UICollectionViewController {

    var contactDatasource: FUIFirestoreCollectionViewDataSource!
    
    public init(itemSize: CGSize) {
        
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.itemSize = itemSize
        collectionViewLayout.scrollDirection = .horizontal
        collectionViewLayout.minimumInteritemSpacing = 15
        
        super.init(collectionViewLayout: collectionViewLayout)

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
        
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(CKContactCollectionCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        
        // Do any additional setup after loading the view.
    }
    
    deinit {
        if(self.contactDatasource != nil) {
            self.contactDatasource.unbind()
        }
    }
    
    public func loadData(order: CKContactOrder? = CKContactOrder.name) {
        let query = ConversationKit.shared.db.collection(ConversationKit.shared.clientKey).document("data").collection("contacts").order(by: order!.rawValue, descending: true)
        self.bindQuery(query: query)

    }

    private func bindQuery(query: Query) {
        
        self.contactDatasource = nil

        self.contactDatasource = self.collectionView.bind(toFirestoreQuery: query, populateCell: { (collectionView, indexPath, documentSnapshot) -> UICollectionViewCell in
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CKContactCollectionCell
            
            let data = documentSnapshot.data()
            
            if(data != nil) {
                var contact = CKContact(id: documentSnapshot.documentID, dictionary: data!)
                
                let absent = Bool.random()
                if(absent) {
                    
                    let voiceMessage = Bool.random()
                    if(voiceMessage == false) {
                        let absenceMessage = CKContactStatusContent(id: "test", dictionary: ["statusContent.contentURL": "users/8oLNwkp4gAZQFaCYBTyaIRmheGH3/E73723F4-B601-4DA2-A989-BFE9F27EF648", "statusContent.contentType": "video/mp4", "statusContent.duration": 17.3, "statusContent.recorded": Timestamp(date: Date()), "statusContent.transcript": ""])
                        contact!.statusContent = absenceMessage
                    }
                    else {
                        let absenceMessage = CKContactStatusContent(id: "test", dictionary: ["statusContent.contentURL": "users/8oLNwkp4gAZQFaCYBTyaIRmheGH3/3AE7ADC0-D5A1-4DA5-B2D1-4C96B04EDC93", "statusContent.contentType": "audio/m4a", "statusContent.duration": 17.66, "statusContent.recorded": Timestamp(date: Date()), "statusContent.transcript": ""])
                        contact!.statusContent = absenceMessage
                    }
                    
                }
                
                cell.contact = contact
            }
            
            return cell
            
        })
        
    }


    override public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = self.collectionView.cellForItem(at: indexPath) as! CKContactCollectionCell
        guard let contact = cell.contact else { return }
        
        print("CKContactCollectionViewController:didSelect "+contact.name)
        
        CKContactDetailsViewController.open(contact: contact, modal: true)
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
