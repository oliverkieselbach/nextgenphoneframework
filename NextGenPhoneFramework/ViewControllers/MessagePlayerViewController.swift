//
//  MessagePlayerViewController.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 25.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseFirestore
import AVKit

public class VideoMessagePlayerViewController: MessagePlayerViewController {

    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var videoTimer: Timer!

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.progressSlider.addTarget(self, action: #selector(self.sliderValueChanged(sender:)), for: .valueChanged)
        
    }

    func play(url: URL) {
        DispatchQueue.main.async {
            if(url != nil) {
                self.initVideoPlayer(url: url)
                self.progressSlider.value = 0.0
                self.avPlayer.play()
                /*
                 self.imageView.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: (self.message.mediaURL!)+"_thumbnail"), placeholderImage: nil) { (image, error, cache, ref) in
                 }*/
            }
        }
    }
    
    @objc public override func playPauseAction() {
        
        print("Play Action")
        
        if(self.avPlayer == nil) {
            
            if(message.mediaURL != "") {
                self.play(url: URL(string: message.mediaURL!)!)
            }
            else {
                ConversationKit.shared.storage.reference(withPath: self.message.mediaRef!).downloadURL { (url, error) in
                    if(error == nil) {
                        self.play(url: url!)
                    }
                }
            }
            
        }
        else {
            if(playPauseButton.isSelected) {
                self.videoTimer.invalidate()
                self.avPlayer.pause()
            }
            else {
                self.videoTimer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateVideoSlider), userInfo: nil, repeats: true)
                self.avPlayer.play()
            }
            self.playPauseButton.isSelected = !self.playPauseButton.isSelected
        }
        
        


    }
    
    func initVideoPlayer(url: URL) {
        
        self.avPlayer = AVPlayer(url: url)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: Notification.Name.AVPlayerItemDidPlayToEndTime,
                                               object: avPlayer?.currentItem)
        
        let playerLayer = AVPlayerLayer(player: avPlayer)
        playerLayer.videoGravity = .resizeAspectFill
        playerLayer.frame = self.imageView.bounds
        self.imageView.layer.addSublayer(playerLayer)
        //self.view.layer.insertSublayer(playerLayer, at: 1) // 1 because of the gradientLayer that is at 0

        /*
        self.avPlayer.addBoundaryTimeObserver(forTimes: [NSValue(time: CMTime(seconds: 0.1, preferredTimescale: 10))], queue: DispatchQueue.main) {
            
        }*/
        
        self.avPlayer.addBoundaryTimeObserver(forTimes: [NSValue(time: CMTime(seconds: 0.1, preferredTimescale: 10))], queue: DispatchQueue.main) {
            
            self.videoTimer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateVideoSlider), userInfo: nil, repeats: true)

            let duration = self.avPlayer.currentItem?.duration
            self.progressSlider.maximumValue = Float(CMTimeGetSeconds(duration!))
            self.playPauseButton.isSelected = true
            
            let totalSeconds = Int(Float(CMTimeGetSeconds(duration!)))
            let seconds = totalSeconds % 60
            self.durationLabel.text = String(format:"%2i sec", seconds)



        }

        
        
    }
    
    @objc func sliderValueChanged(sender: UISlider) {
        print("Slider Value changed")
        self.avPlayer.seek(to: CMTime(seconds: Double(sender.value), preferredTimescale: 10))
    }
    
    @objc func updateVideoSlider() {
        
        self.progressSlider.value = Float(CMTimeGetSeconds(self.avPlayer.currentTime()))
    }

    
    @objc func playerItemDidReachEnd(notification: Notification) {
        if let playerItem: AVPlayerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
            self.playPauseButton.isSelected = false
            self.videoTimer.invalidate()
            self.progressSlider.value = 0
        }
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if(avPlayer != nil) {
            self.avPlayer.pause()
            self.playPauseButton.isSelected = false
            self.videoTimer.invalidate()
            self.progressSlider.value = 0
        }
        
        super.dismiss(animated: flag, completion: completion)
    }


    
}

public class VoiceMessagePlayerViewController: MessagePlayerViewController {
    
    var audioPlayer: AVAudioPlayer!
    var audioTimer: Timer!
    
    var siriWave: PXSiriWave = {
        let wave = PXSiriWave(frame: .zero)
        //    siriWave.colors = [NSArray arrayWithObjects: [ViewController colorFromHexCode: @"#2085fc"], [ViewController colorFromHexCode: @"#5efca9"], [ViewController colorFromHexCode: @"#fd4767"], nil];
        wave.colors = [UIColor(0x2085fc), UIColor(0x5efca9), UIColor(0xfd4767)]
        
        return wave
        
    }()

    
    public override func viewDidLoad() {
        super.viewDidLoad()

        self.siriWave.translatesAutoresizingMaskIntoConstraints = false
        //self.siriWave.frame = CGRect(x: 15, y: self.view.center.y - 50, width: self.view.frame.width - 30, height: 100)
        self.siriWave.backgroundColor = .clear
        self.view.addSubview(self.siriWave)
        self.siriWave.leftAnchor.constraint(equalTo: self.imageView.leftAnchor, constant: 0).isActive = true
        self.siriWave.rightAnchor.constraint(equalTo: self.imageView.rightAnchor, constant: 0).isActive = true
        self.siriWave.centerYAnchor.constraint(equalTo: self.imageView.centerYAnchor, constant: 0).isActive = true
        self.siriWave.heightAnchor.constraint(equalToConstant: 150).isActive = true
        self.siriWave.configure()


        self.imageView.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: self.message.senderDetails.profileImageRef), placeholderImage: nil, completion: { (image, error, cache, ref) in
            
            if(error == nil) {
            }
        })

    }
    
    public override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.imageView.addBlurView(style: .dark)

    }
    
    @objc public override func playPauseAction() {
        
        print("Play Action")
        
        if(message.mediaRef == "" && message.mediaURL == "") {
            self.dismiss(animated: true)
            return
        }
        
        if(message.mediaURL != "") {
            
            self.play(audioURL: URL(string: message.mediaURL!)!)
            
        }
        else {
            ConversationKit.shared.storage.reference(withPath: message.mediaRef!).downloadURL { (url, error) in
                
                if(error != nil) {
                    print(error?.localizedDescription)
                    return
                }
                self.play(audioURL: url!)
                
            }
        }
        
        
    }
    
    func play(audioURL: URL) {
        if(self.audioPlayer == nil) {
            do {
                
                let audioData = try Data(contentsOf: audioURL)
                self.audioPlayer = try AVAudioPlayer(data: audioData, fileTypeHint: "m4a")
                self.progressSlider.value = 0.0
                self.progressSlider.maximumValue = Float((self.audioPlayer?.duration)!)
                self.progressSlider.addTarget(self, action: #selector(self.sliderValueChanged(sender:)), for: .valueChanged)
                self.audioPlayer.isMeteringEnabled = true
                let displayLink = CADisplayLink(target: self, selector: #selector(self.updateMetersFromPlayback))
                displayLink.add(to: RunLoop.current, forMode: RunLoop.Mode.common)
                self.audioPlayer.delegate = self
                self.audioPlayer.prepareToPlay()
                
                let totalSeconds = Int(self.audioPlayer.duration)
                let seconds = totalSeconds % 60
                self.durationLabel.text = String(format:"%2i sec", seconds)
                
                
            } catch {
                print(error)
            }
        }
        
        if(!self.playPauseButton!.isSelected) {
            self.playPauseButton!.isSelected = !self.playPauseButton!.isSelected
            self.audioPlayer.play()
            self.audioTimer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateAudioSlider), userInfo: nil, repeats: true)
        }
        else {
            self.playPauseButton!.isSelected = !self.playPauseButton!.isSelected
            self.audioTimer.invalidate()
            self.siriWave.update(withLevel: 0)
            
            self.audioPlayer.pause()
        }

    }
    
    @objc func sliderValueChanged(sender: UISlider) {
        print("Slider Value changed")
        self.audioPlayer.currentTime = TimeInterval(exactly: sender.value)!
    }
    
    @objc func updateAudioSlider() {
        self.progressSlider.value = Float(self.audioPlayer.currentTime)
    }
    
    @objc func updateMetersFromPlayback() {
        if(audioPlayer != nil) {
            audioPlayer.updateMeters()
            var normalizedValue: Float = 0.0
            if(audioPlayer.averagePower(forChannel: 0) < -60 || audioPlayer.averagePower(forChannel: 0) / 20 == 0) {
                normalizedValue = 0
            }
            else {
                normalizedValue = pow(10, audioPlayer.averagePower(forChannel: 0) / 20)
            }
            
            self.siriWave.update(withLevel: CGFloat(normalizedValue))
        }
    }
    
    public override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        
        self.playPauseButton!.isSelected = !self.playPauseButton!.isSelected
        if(self.audioTimer != nil) {
            self.audioTimer.invalidate()
        }
        self.siriWave.update(withLevel: 0)
        if(self.audioPlayer != nil) {
            self.audioPlayer.pause()
        }

        super.dismiss(animated: flag, completion: completion)
    }


    
}

extension VoiceMessagePlayerViewController: AVAudioPlayerDelegate {
    
    public func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print(error?.localizedDescription ?? "Unknown Error decoding audio file")
    }
    
    public func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.audioPlayer = nil
        self.playPauseButton.isSelected = false
        self.progressSlider.value = 0
        self.audioTimer.invalidate()
        self.siriWave.update(withLevel: 0)
        
    }

    
}

public class MessagePlayerViewController: UIViewController {
    
    public var message: CKMessage! {
        didSet {
            
            let totalSeconds = Int(message.duration)
            let seconds = totalSeconds % 60
            let mediaDuration = String(format:"%2i sec", seconds)
            self.durationLabel.text = mediaDuration
            
            self.topicActionView.backgroundColor = self.view.tintColor
            self.topicAction.setTitle(message.topic == "" ? message.topicAction : message.topic, for: .normal)
            
            self.avatar.contactWithNoStatus = message.senderDetails
            self.senderName.text = message.senderDetails.name

            
        }
    }
    
    var avatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        return v
        
    }()
    
    var senderName: UILabel = {
        
        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.numberOfLines = 2
        
        return label
        
    }()

    var imageView: UIImageView! = {

        let v = UIImageView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.contentMode = .scaleAspectFill
        v.layer.cornerRadius = 8
        v.clipsToBounds = true
        
        return v
        
    }()
    
    
    var playPauseButton: UIButton! = {
        
        let button = UIButton(type: .custom)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        button.backgroundColor = .clear
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 20
        
        button.tintColor = .black
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.setImage(UIImage(imageLiteralResourceName: "pause").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .selected)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        return button

    }()
    
    var durationLabel: UILabel = {

        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        
        return label
        
        
    }()
    
    var progressSlider: UISlider = {
        
        let slider = UISlider(frame: .zero)
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.minimumTrackTintColor = .black
        slider.maximumTrackTintColor = .black
        slider.thumbTintColor = .black
        
        let thumbImage = UIImage(imageLiteralResourceName: "circle_filled").withRenderingMode(.alwaysTemplate).scaleImageToFitSize(size: CGSize(width: 10, height: 10))
        slider.setThumbImage(thumbImage, for: .normal)
        slider.setThumbImage(thumbImage, for: .highlighted)
        return slider
        
    }()


    var topicAction: UIButton = {

        var button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        return button
        
    }()
    
    var topicActionView: UIView = {
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 8

        return v

    }()
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        topicActionView.addSubview(self.topicAction)
        topicAction.topAnchor.constraint(equalTo: topicActionView.topAnchor, constant: 3).isActive = true
        topicAction.leftAnchor.constraint(equalTo: topicActionView.leftAnchor, constant: 5).isActive = true
        topicAction.rightAnchor.constraint(equalTo: topicActionView.rightAnchor, constant: -5).isActive = true
        topicAction.bottomAnchor.constraint(equalTo: topicActionView.bottomAnchor, constant: -3).isActive = true

        
        
        //self.view.addSubview(topicActionView)
        self.view.addSubview(self.imageView)
        self.view.addSubview(self.playPauseButton)
        self.view.addSubview(self.durationLabel)
        self.view.addSubview(self.progressSlider)
        self.view.addSubview(self.avatar)
        self.view.addSubview(self.senderName)

        self.avatar.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 15).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true

        self.senderName.centerYAnchor.constraint(equalTo: self.avatar.centerYAnchor, constant: 0).isActive = true
        self.senderName.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 5).isActive = true
        
        //topicActionView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        //topicActionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 15).isActive = true
        
        self.imageView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        self.imageView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true
        self.imageView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 80).isActive = true
        self.imageView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -80).isActive = true
        
        self.progressSlider.centerYAnchor.constraint(equalTo: self.playPauseButton.centerYAnchor, constant: 0).isActive = true
        self.progressSlider.leftAnchor.constraint(equalTo: self.playPauseButton.rightAnchor, constant: 10).isActive = true
        
        self.progressSlider.rightAnchor.constraint(equalTo: self.durationLabel.leftAnchor, constant: -10).isActive = true
        
        self.playPauseButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -15).isActive = true
        self.playPauseButton.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 15).isActive = true
        
        self.durationLabel.centerYAnchor.constraint(equalTo: self.playPauseButton.centerYAnchor, constant: 0).isActive = true
        self.durationLabel.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: -15).isActive = true

        self.playPauseButton.addTarget(self, action: #selector(self.playPauseAction), for: .touchDown)
        
        // Do any additional setup after loading the view.
    }
    
    
    @objc public func playPauseAction() {
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
