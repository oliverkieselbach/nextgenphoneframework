//
//  B2CContact.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 02.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import LetterAvatarKit
import FirebaseUI

public class CKContactView: UIView {
    var availableColor = UIColor(displayP3Red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
    var notAvailableColor = UIColor(displayP3Red: 231/255, green: 76/255, blue: 60/255, alpha: 1)
    
    public var contactWithNoStatus: CKContactBasic! {
        didSet {
            self.contact = CKContact(personEmbedded: contactWithNoStatus)
        }
    }
    
    public var contact: CKContact! {
        didSet {
            
            if(contact != nil) {
                let letterKit = LetterAvatarBuilderConfiguration()
                letterKit.username = contact.name
                letterKit.backgroundColors = [.gray] //[contact.status == .available ? self.availableColor : self.notAvailableColor]
                letterKit.lettersColor = UIColor.white
                letterKit.lettersFont = UIFont.systemFont(ofSize: 17)
                
                if(contact.profileImageRef == "") {
                    self.avatar.image = UIImage.makeLetterAvatar(withConfiguration: letterKit)
                    
                }
                else {
                    self.avatar.sd_setImage(with: ConversationKit.shared.storage.reference().child((contact.profileImageRef)), placeholderImage: UIImage.makeLetterAvatar(withConfiguration: letterKit)) { (image, error, cache, ref) in
                        
                        if(error != nil) {
                            print(error?.localizedDescription ?? "Error")
                        }
                    }
                }
                
                
                
                let status: CKContactStatus! = contact.status!
                
                
                switch status {
                case .available?:
                    //self.layer.borderColor = self.availableColor.cgColor
                    self.dotsLayer.strokeColor = self.availableColor.cgColor
                    
                case .notAvailable?:
                    //self.layer.borderColor = self.notAvailableColor.cgColor
                    self.dotsLayer.strokeColor = self.notAvailableColor.cgColor
                    
                default:
                    //self.layer.borderColor = UIColor.clear.cgColor
                    self.dotsLayer.strokeColor = UIColor.clear.cgColor
                }
                
                
                let one : NSNumber = 1
                let two : NSNumber = self.contact.statusContent == nil ? 5 : 0
                self.dotsLayer.lineDashPattern = [one,two]
                
                
                self.layoutIfNeeded()
                
                self.needsUpdateConstraints()
            }
            
        }
    }
    
    var dotsLayer: CAShapeLayer!
    
    var avatarStatus: UIView = {
        
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .clear
        
        return v
        
    }()
    
    var avatar: UIImageView = {
        
        let imageView = UIImageView(frame: CGRect.zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.contentMode = .scaleAspectFill
        
        return imageView
        
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        
        self.addSubview(self.avatarStatus)
        self.addSubview(self.avatar)
        self.avatar.topAnchor.constraint(equalTo: self.topAnchor, constant: 6).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 6).isActive = true
        self.avatar.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -6).isActive = true
        self.avatar.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -6).isActive = true
        
        self.avatarStatus.topAnchor.constraint(equalTo: self.topAnchor, constant: 1).isActive = true
        self.avatarStatus.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 1).isActive = true
        self.avatarStatus.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -1).isActive = true
        self.avatarStatus.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1).isActive = true
        
        self.dotsLayer = CAShapeLayer()
        
        self.avatarStatus.clipsToBounds = false
        
        self.clipsToBounds = true
        self.avatar.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.avatar.layer.cornerRadius = self.avatar.frame.size.height * 0.5
        //self.layer.cornerRadius = self.layer.frame.height * 0.5
        //self.layer.borderWidth = max(2, self.layer.frame.height * 0.02) //self.layer.frame.height * 0.02
        
        circleOfDots()
        
    }
    
    public func addTapTarget(target: Any?, action: Selector?) {
        let tapGesture = UITapGestureRecognizer(target: target, action: action)
        self.addGestureRecognizer(tapGesture)
    }
    
    func circleOfDots() {
        let circlePath = UIBezierPath(arcCenter: self.avatarStatus.center, radius: self.avatarStatus.frame.height * 0.5, startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let shapeLayer = self.dotsLayer!
        
        shapeLayer.path = circlePath.cgPath
        shapeLayer.position = CGPoint(x: (self.avatarStatus.bounds.midX - self.avatarStatus.center.x), y: (self.avatarStatus.bounds.midY - self.avatarStatus.center.y)) // CGPointMake(CGRectGetMidX(self.bounds)-200 ,CGRectGetMidY(self.bounds)-200 )
        //change the fill color
        shapeLayer.fillColor = UIColor.clear.cgColor
        //you can change the stroke color
        if(shapeLayer.strokeColor == nil) {
            shapeLayer.strokeColor = UIColor.clear.cgColor
        }
        //you can change the line width
        shapeLayer.lineWidth = max(2, self.frame.height * 0.02) //6.0
        if(shapeLayer.lineDashPattern == nil) {
            let one : NSNumber = 1
            let two : NSNumber = 5
            shapeLayer.lineDashPattern = [one,two]
        }
        shapeLayer.lineCap = CAShapeLayerLineCap.square
        self.avatarStatus.layer.addSublayer(shapeLayer)
    }
    
}

