//
//  CKContactViewController.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 03.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit

public protocol CKContactViewDelegate {
    
    func didTap(contact: CKContact)
    
}

public class CKContactViewController: UIViewController {

    
    var contactView: CKContactView! = {

        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
        
        
    }()
    
    var contactName: UILabel = {

        let label = UILabel()
    
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.textAlignment = .center
        label.numberOfLines = 0
        
        return label
        
    }()

    var width: CGFloat = 100
    
    public var delegate: CKContactViewDelegate?
    
    public init(phoneNumber: String, width: CGFloat) {
        
        super.init(nibName: nil, bundle: nil)
        self.loadData(mobilePhoneNumber: phoneNumber)
        self.width = width
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.widthAnchor.constraint(equalToConstant: self.width).isActive = true
        
        self.view.addSubview(self.contactView)
        self.view.addSubview(self.contactName)
        self.contactView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        self.contactView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        self.contactView.widthAnchor.constraint(equalTo: self.view.widthAnchor, constant: 0).isActive = true
        self.contactView.heightAnchor.constraint(equalTo: self.view.widthAnchor, constant: 0).isActive = true

        self.contactName.topAnchor.constraint(equalTo: self.contactView.bottomAnchor, constant: 5).isActive = true
        self.contactName.widthAnchor.constraint(equalTo: self.contactView.widthAnchor, constant: 0).isActive = true
        self.view.bottomAnchor.constraint(equalTo: self.contactName.bottomAnchor, constant: 0).isActive = true
        
        
        self.contactView.addTapTarget(target: self, action: #selector(self.tapAction))
        
        
    }
    
    @objc private func tapAction() {
        if(delegate != nil) {
            self.delegate!.didTap(contact: self.contactView.contact)
        }
    }
    
    
    public func loadData(id: String) {
        
        ConversationKit.shared.db.collection("users").document(id).getDocument { (document, error) in
            if let document = document, document.exists {
                
                let data = document.data()
                if(data != nil) {
                    let contact = CKContact(id: (document.documentID), dictionary: data!)
                    self.contactView.contact = contact
                    self.contactName.text = contact?.name
                }
                else {
                    print("No Data for Contact with Id "+id)
                }

                
            } else {
                print("Contact not found with id "+id)
            }
        }
        
    }
    
    public func loadData(mobilePhoneNumber: String) {
        
        ConversationKit.shared.db.collection("users").whereField("phoneNumber", isEqualTo: mobilePhoneNumber).limit(to: 1).getDocuments { (querySnapshot, error) in
            guard let snapshot = querySnapshot else {
                print(error?.localizedDescription)
                
                return
            }
            
            let document = snapshot.documents.first
            if(document != nil) {
                let data = document?.data()
                if(data != nil) {
                    let contact = CKContact(id: (document?.documentID)!, dictionary: data!)
                    self.contactView.contact = contact
                    self.contactName.text = contact?.name
                }
                else {
                    print("No Data for Contact with "+mobilePhoneNumber)
                }
            }
            else {
                print("No Contact Found with "+mobilePhoneNumber)
            }
        }
        
    }

}
