//
//  CKContactTableViewController.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 11.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseUI

private let reuseIdentifier = "ckContactTableViewCell"

public class CKContactTableViewCell: UITableViewCell {
    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
            self.contactName.text = contact.name
            self.desc.text = contact.description
        }
    }
    
    var avatar: CKContactView = {
        
        let a = CKContactView(frame: .zero)
        a.translatesAutoresizingMaskIntoConstraints = false
        a.heightAnchor.constraint(equalToConstant: 60).isActive = true //100
        a.widthAnchor.constraint(equalToConstant: 60).isActive = true //100
        
        return a
    }()
    
    var contactName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.bold)
        label.textAlignment = .left
        label.numberOfLines = 1
        
        return label
        
    }()
    
    var desc: UILabel! = {
        
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textAlignment = .left
        label.textColor = .gray
        
        label.numberOfLines = 2
        
        return label
        
    }()

    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.contentView.addSubview(self.avatar)
        self.contentView.addSubview(self.contactName)
        self.contentView.addSubview(self.desc)
        //self.avatar.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 3).isActive = true
        self.avatar.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor, constant: 0).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        

        self.contactName.topAnchor.constraint(equalTo: self.avatar.topAnchor, constant: 2).isActive = true
        self.contactName.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 8).isActive = true
        self.contactName.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        
        self.desc.topAnchor.constraint(equalTo: self.contactName.bottomAnchor, constant: 8).isActive = true
        self.desc.leftAnchor.constraint(equalTo: self.contactName.leftAnchor, constant: 0).isActive = true
        self.desc.rightAnchor.constraint(equalTo: self.contactName.rightAnchor, constant: 0).isActive = true

        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

public class CKContactTableViewController: UITableViewController {

    private var contactDatasource: FUIFirestoreTableViewDataSource!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = 80

        self.tableView.delegate = self
        // Register cell classes
        self.tableView.register(CKContactTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
    }
    
    deinit {
        if(self.contactDatasource != nil) {
            self.contactDatasource.unbind()
        }
    }
    
    public func loadData(order: CKContactOrder? = CKContactOrder.name) {
        let query = ConversationKit.shared.db.collection(ConversationKit.shared.clientKey).document("data").collection("contacts").order(by: order!.rawValue, descending: true)
        self.bindQuery(query: query)
        
    }
    
    private func bindQuery(query: Query) {
        
        self.contactDatasource = nil
        
        self.contactDatasource = self.tableView.bind(toFirestoreQuery: query, populateCell: { (collectionView, indexPath, documentSnapshot) -> UITableViewCell in
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CKContactTableViewCell
            
            let data = documentSnapshot.data()
            
            if(data != nil) {
                var contact = CKContact(id: documentSnapshot.documentID, dictionary: data!)
                
                let absent = Bool.random()
                if(absent) {
                    
                    let voiceMessage = Bool.random()
                    if(voiceMessage == false) {
                        let absenceMessage = CKContactStatusContent(id: "test", dictionary: ["statusContent.contentURL": "users/8oLNwkp4gAZQFaCYBTyaIRmheGH3/E73723F4-B601-4DA2-A989-BFE9F27EF648", "statusContent.contentType": "video/mp4", "statusContent.duration": 17.3, "statusContent.recorded": Timestamp(date: Date()), "statusContent.transcript": ""])
                        contact!.statusContent = absenceMessage
                    }
                    else {
                        let absenceMessage = CKContactStatusContent(id: "test", dictionary: ["statusContent.contentURL": "users/8oLNwkp4gAZQFaCYBTyaIRmheGH3/3AE7ADC0-D5A1-4DA5-B2D1-4C96B04EDC93", "statusContent.contentType": "audio/m4a", "statusContent.duration": 17.66, "statusContent.recorded": Timestamp(date: Date()), "statusContent.transcript": ""])
                        contact!.statusContent = absenceMessage
                    }
                    
                }
                
                cell.contact = contact
            }
            
            return cell
            
        })
        
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = self.tableView.cellForRow(at: indexPath) as! CKContactTableViewCell
        guard let contact = cell.contact else { return }

        print("CKContactTableViewController:didSelect "+contact.name)

        CKContactDetailsViewController.open(contact: contact, modal: true)
        
    }



}
