//
//  CKContactDetailsViewController.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 12.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import FirebaseUI
import BonsaiController
import AVKit
import DateToolsSwift
import SwiftRichString


public class TopicMessageView: UIView {
    
    public var message: CKMessage! {
        didSet {
            
            self.avatar.contactWithNoStatus = message.senderDetails
            self.senderName.text = message.senderDetails.name
            
            var messageTypeText = ""
            switch message.messageType! {
            case .video:
                messageTypeText = "Video Message"
                break
            case .voice:
                messageTypeText = "Voice Message"
                break
            case .call:
                messageTypeText = "Video Call"
                break

            default:
                messageTypeText = "Unknown"
            }
            
            self.messageType.text = messageTypeText
            self.isReadIcon.tintColor = message.isRead ? .white : UIColor.white.withAlphaComponent(0.2)

            let totalSeconds = Int(message.duration)
            let seconds = totalSeconds % 60
            let mediaDuration = String(format:"%2i sec", seconds)
            self.durationLabel.text = mediaDuration

        }
    }
    
    var avatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        return v
        
    }()
    
    var senderName: UILabel = {
        
        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.numberOfLines = 2
        
        return label
        
    }()

    var messageType: UILabel = {
        
        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        label.numberOfLines = 1
        
        return label
        
    }()

    var playButton: UIButton = {
        
        let button = UIButton(type: .custom)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        button.backgroundColor = .clear
        button.layer.borderColor = UIColor.darkGray.cgColor
        button.layer.borderWidth = 1
        button.layer.cornerRadius = 15
        
        button.tintColor = .darkGray
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        return button
        
        
    }()
    
    var isReadIcon: UIImageView = {
        
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.heightAnchor.constraint(equalToConstant: 14).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 14).isActive = true
        imageView.image = UIImage(imageLiteralResourceName: "isRead").withRenderingMode(.alwaysTemplate)
        
        return imageView
        
    }()
    
    var durationLabel: UILabel = {
        
        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        
        return label
        
        
    }()

    

    
    public var playMessageAction: ((CKMessage) -> Void)?
    

    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        //self.addSubview(self.avatar)
        //self.addSubview(self.senderName)
        self.addSubview(self.playButton)
        self.addSubview(self.messageType)
        self.addSubview(self.isReadIcon)
        self.addSubview(self.durationLabel)
        
        self.backgroundColor = self.tintColor.withAlphaComponent(0.1) //UIColor(0xe4f1fe) //UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        self.playButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.playButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        
        self.messageType.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        self.messageType.leftAnchor.constraint(equalTo: self.playButton.rightAnchor, constant: 10).isActive = true
        
        self.isReadIcon.centerYAnchor.constraint(equalTo: self.messageType.centerYAnchor, constant: 0).isActive = true
        self.isReadIcon.leftAnchor.constraint(equalTo: self.messageType.rightAnchor, constant: 5).isActive = true

        self.durationLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        self.durationLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        
        /*
        self.playButton.tintColor = self.tintColor
        self.playButton.layer.borderColor = self.tintColor.cgColor
        self.durationLabel.textColor = self.tintColor
        self.messageType.textColor = self.tintColor
        */
        
        self.playButton.addTarget(self, action: #selector(self.playAction), for: .touchDown)
        
    }
    
    @objc func playAction() {
        print("MessageContentView.playAction")
        if(self.playMessageAction != nil) {
            self.playMessageAction!(self.message)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
    
}

class AnonmyousView: UIView {
    
    var messageLabel: UILabel = {
        
        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        label.textColor = .black
        label.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textAlignment = .center
        label.layer.cornerRadius = 0
        label.text = "You are not signed in yet."
        
        return label
        
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        let containerView = UIView(frame: .zero)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.layer.cornerRadius = 8
        containerView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        self.addSubview(containerView)
        containerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        containerView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        containerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        containerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true

        containerView.addSubview(self.messageLabel)
        
        self.messageLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor, constant: 0).isActive = true
        self.messageLabel.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 15).isActive = true
        self.messageLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor, constant: -15).isActive = true

        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class NoConversationView: UIView {
    
    
    var contact: CKContact! {
        didSet {
            self.messageLabel.text = "You have no conversation with "+contact.name
        }
    }
    
    var messageLabel: UILabel = {

        var label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 3
        label.textColor = .black
        label.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        label.textAlignment = .center
        label.layer.cornerRadius = 20
        label.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        
        return label
        
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.messageLabel)
        self.messageLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10).isActive = true
        self.messageLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10).isActive = true
        
        self.messageLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.messageLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

public class TopicCell: UITableViewCell {

    public var conversation: CKConversation!
    
    var avatar: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        return v
        
    }()

    public var messageContentView: TopicMessageView = {
        let v = TopicMessageView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 8
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        return v
    }()
    
    var contentContainerView: UIView = {
        
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 8
        
        
        return v
        
    }()
    
    var dateLabel: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        label.textAlignment = .right
        
        return label
    }()

    
}

public class TopicOnlyCell: TopicCell {
    
    static public var reuseId = "topicOnlyCell"
    
    var cellColor: UIColor = UIColor(0xF57C00)
    
    override public var conversation: CKConversation! {
        didSet {
            
            self.intentMessage = conversation.intentMessageContent
            
        }
    }
    
    var intentMessage: CKMessage! {
        didSet {
            
            
        }
    }

    var topicView: CKIntentView = {
        
        let v = CKIntentView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
        
    }()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.cellColor = self.contentView.tintColor
        
        self.contentView.addSubview(self.contentContainerView)
        self.contentContainerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        self.contentContainerView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.contentContainerView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.contentContainerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true

        self.contentContainerView.addSubview(self.messageContentView)
        self.contentContainerView.addSubview(self.topicView)
        self.contentContainerView.addSubview(self.dateLabel)
        self.contentContainerView.addSubview(self.avatar)
        
        self.avatar.topAnchor.constraint(equalTo: contentContainerView.topAnchor, constant: 0).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.contentContainerView.leftAnchor, constant: 0).isActive = true
        
        self.topicView.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 5).isActive = true
        self.topicView.topAnchor.constraint(equalTo: self.avatar.topAnchor, constant: 3).isActive = true
        self.topicView.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: -15).isActive = true
        
        self.messageContentView.leftAnchor.constraint(equalTo: self.topicView.leftAnchor, constant: 0).isActive = true
        self.messageContentView.topAnchor.constraint(equalTo: self.topicView.bottomAnchor, constant: 10).isActive = true
        self.messageContentView.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: 0).isActive = true
        
        self.dateLabel.topAnchor.constraint(equalTo: self.messageContentView.bottomAnchor, constant: 5).isActive = true
        self.dateLabel.bottomAnchor.constraint(equalTo: self.contentContainerView.bottomAnchor, constant: 0).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: 0).isActive = true

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

public class TopicActionCell: TopicCell {
    
    static public var reuseId = "topicActionCell"

    var cellColor: UIColor = UIColor(0xF57C00)
    
    override public var conversation: CKConversation! {
        didSet {
            
            self.intentMessage = conversation.intentMessageContent
            self.actionMessage = conversation.lastMessageContent
            
            //self.contentContainerView.backgroundColor = .white // self.cellColor
        }
    }
    
    var intentMessage: CKMessage! {
        didSet {
            

        }
    }
    
    var actionMessage: CKMessage! {
        didSet {
            
            let style = StyleGroup(base: Style {
                $0.font = UIFont.systemFont(ofSize: 15, weight: .regular)
                $0.color = UIColor.black
                $0.lineSpacing = 2
                }, [
                    "action": Style {
                        $0.font = UIFont.systemFont(ofSize: 13, weight: .bold)
                        $0.backColor = self.actionMessage.topicActionDetails.getColor().backgroundColor
                        $0.color = self.actionMessage.topicActionDetails.getColor().tintColor
                        $0.kerning = Kerning.adobe(-20)
                    }])


            self.actionView.message = actionMessage
            self.actionView.style(styleGroup: style)
            
            self.messageContentView.message = actionMessage
            
            self.dateLabel.text = actionMessage.date.timeAgoSinceNow
            self.avatar.contactWithNoStatus = actionMessage.senderDetails

        }
    }
    
    var actionView: CKIntentActionView = {

        let v = CKIntentActionView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
        
    }()


    var topicView: CKIntentView = {

        let v = CKIntentView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v

    }()
    

    var avatarIntent: CKContactView = {
        
        let v = CKContactView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.heightAnchor.constraint(equalToConstant: 50).isActive = true
        v.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        return v
        
    }()

    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.cellColor = UIColor(0xe74c3c)
        
        let dashedView = DashedLineView(frame: .zero)
        dashedView.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(self.contentContainerView)
        self.contentContainerView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 10).isActive = true
        self.contentContainerView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 15).isActive = true
        self.contentContainerView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -15).isActive = true
        self.contentContainerView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10).isActive = true
        
        
        
        self.contentContainerView.addSubview(self.topicView)
        self.contentContainerView.addSubview(self.messageContentView)
        self.contentContainerView.addSubview(self.actionView)
        self.contentContainerView.addSubview(self.dateLabel)
        self.contentContainerView.addSubview(self.avatar)
        self.contentContainerView.addSubview(dashedView)
        self.contentContainerView.addSubview(self.avatarIntent)

        self.avatarIntent.centerXAnchor.constraint(equalTo: self.avatar.centerXAnchor, constant: 0).isActive = true
        self.avatarIntent.topAnchor.constraint(equalTo: self.contentContainerView.topAnchor, constant: 0).isActive = true
        
        self.topicView.leftAnchor.constraint(equalTo: self.avatarIntent.rightAnchor, constant: 5).isActive = true
        self.topicView.topAnchor.constraint(equalTo: self.avatarIntent.topAnchor, constant: 3).isActive = true
        self.topicView.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: 0).isActive = true

        dashedView.topAnchor.constraint(equalTo: self.avatarIntent.bottomAnchor, constant: 5).isActive = true
        dashedView.leftAnchor.constraint(equalTo: self.avatar.centerXAnchor, constant: 0).isActive = true
        dashedView.widthAnchor.constraint(equalToConstant: 10).isActive = true
        dashedView.heightAnchor.constraint(equalToConstant: 20).isActive = true

        self.avatar.topAnchor.constraint(equalTo: dashedView.bottomAnchor, constant: 5).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.contentContainerView.leftAnchor, constant: 0).isActive = true
        
        self.actionView.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 5).isActive = true
        self.actionView.topAnchor.constraint(equalTo: self.avatar.topAnchor, constant: 3).isActive = true
        self.actionView.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: 0).isActive = true
        
        self.messageContentView.leftAnchor.constraint(equalTo: self.actionView.leftAnchor, constant: 0).isActive = true
        self.messageContentView.topAnchor.constraint(equalTo: self.actionView.bottomAnchor, constant: 10).isActive = true
        self.messageContentView.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: 0).isActive = true
 
        self.dateLabel.topAnchor.constraint(equalTo: self.messageContentView.bottomAnchor, constant: 5).isActive = true
        self.dateLabel.bottomAnchor.constraint(equalTo: self.contentContainerView.bottomAnchor, constant: 0).isActive = true
        self.dateLabel.rightAnchor.constraint(equalTo: self.contentContainerView.rightAnchor, constant: 0).isActive = true

        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


struct CKContactDetailsViewConfig {
    
    var backgroundColor: UIColor
    
}

class ContactVideoStatusView: UIView {
    
    var statusContent: CKContactStatusContent! {
        didSet {
            
            self.thumbnail.sd_setImage(with: ConversationKit.shared.storage.reference(withPath: (statusContent.contentURL)+"_thumbnail"))
            
        }
    }

    var thumbnail: UIImageView = {

        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.layer.cornerRadius = 0
        imageView.clipsToBounds = true
        
        return imageView
        
    }()
    
    var playButton: UIButton = {

        let button = UIButton(type: .custom)

        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        button.backgroundColor = .clear
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 20
        
        button.tintColor = .white
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        return button
        
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.thumbnail)
        self.addSubview(self.playButton)
        self.thumbnail.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.thumbnail.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.thumbnail.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.thumbnail.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true

        self.playButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.playButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

class ContactAudioStatusView: UIView {
    
    var statusContent: CKContactStatusContent! {
        didSet {
            
            self.thumbnail.backgroundColor = UIColor(0xFAFAFA)
            
        }
    }
    
    var thumbnail: UIImageView = {
        
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.layer.cornerRadius = 0
        imageView.clipsToBounds = true
        
        return imageView
        
    }()
    
    var playButton: UIButton = {
        
        let button = UIButton(type: .custom)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.widthAnchor.constraint(equalToConstant: 40).isActive = true
        
        button.backgroundColor = .clear
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 20
        
        button.tintColor = .black
        button.setImage(UIImage(imageLiteralResourceName: "play").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        return button
        
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.thumbnail)
        self.addSubview(self.playButton)
        self.thumbnail.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.thumbnail.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.thumbnail.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.thumbnail.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
        self.playButton.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        self.playButton.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


class ContactNoStatusView: UIView {
    
    var message: UILabel = {

        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false

        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        label.textAlignment = .center
        label.numberOfLines = 2
        
        return label
        
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(self.message)
        
        self.message.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        self.message.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        self.message.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true

        self.message.text = "Sorry, I did not create a status message"
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class ContactHeaderView: UIView {
    
    var contact: CKContact! {
        didSet {
            self.avatar.contact = contact
            self.name.text = contact.name
            self.desc.text = contact.description
            
            if(self.contact.statusContent == nil) {
                self.statusContainerView.addSubview(self.noStatus)
                self.noStatus.topAnchor.constraint(equalTo: self.statusContainerView.topAnchor, constant: 0).isActive = true
                self.noStatus.leftAnchor.constraint(equalTo: self.statusContainerView.leftAnchor, constant: 15).isActive = true
                self.noStatus.rightAnchor.constraint(equalTo: self.statusContainerView.rightAnchor, constant: -15).isActive = true
                self.noStatus.heightAnchor.constraint(equalTo: self.statusContainerView.widthAnchor, multiplier: 0.3).isActive = true
                self.noStatus.bottomAnchor.constraint(equalTo: self.statusContainerView.bottomAnchor, constant: 0).isActive = true

            }
            else {
                switch self.contact.statusContent!.contentType! {
                case .video:
                    print("Status Video Content")
                    self.statusContainerView.addSubview(self.videoStatus)
                    self.videoStatus.topAnchor.constraint(equalTo: self.statusContainerView.topAnchor, constant: 0).isActive = true
                    self.videoStatus.leftAnchor.constraint(equalTo: self.statusContainerView.leftAnchor, constant: 0).isActive = true
                    self.videoStatus.rightAnchor.constraint(equalTo: self.statusContainerView.rightAnchor, constant: 0).isActive = true
                    self.videoStatus.heightAnchor.constraint(equalTo: self.videoStatus.widthAnchor, multiplier: 0.75).isActive = true
                    self.videoStatus.bottomAnchor.constraint(equalTo: self.statusContainerView.bottomAnchor, constant: 0).isActive = true
                    
                    self.videoStatus.statusContent = self.contact.statusContent
                    
                case .audio:
                    print("Status Audio Content")
                    self.statusContainerView.addSubview(self.audioStatus)
                    self.audioStatus.topAnchor.constraint(equalTo: self.statusContainerView.topAnchor, constant: 0).isActive = true
                    self.audioStatus.leftAnchor.constraint(equalTo: self.statusContainerView.leftAnchor, constant: 0).isActive = true
                    self.audioStatus.rightAnchor.constraint(equalTo: self.statusContainerView.rightAnchor, constant: 0).isActive = true
                    self.audioStatus.heightAnchor.constraint(equalTo: self.audioStatus.widthAnchor, multiplier: 0.4).isActive = true
                    self.audioStatus.bottomAnchor.constraint(equalTo: self.statusContainerView.bottomAnchor, constant: 0).isActive = true
                    
                    self.audioStatus.statusContent = self.contact.statusContent
                }
            }
        }
    }
    
    var avatar: CKContactView! = {
        
        let view = CKContactView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 70).isActive = true
        view.widthAnchor.constraint(equalToConstant: 70).isActive = true
        
        return view
        
    }()
    
    var name: UILabel! = {
        
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        label.textAlignment = .left
        label.textColor = .black
        
        label.numberOfLines = 2
        
        return label
        
    }()

    var desc: UILabel! = {
        
        var label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.textAlignment = .left
        label.textColor = .gray
        
        label.numberOfLines = 3
        
        return label
        
    }()
    
    var videoStatus: ContactVideoStatusView = {
        let v = ContactVideoStatusView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 8

        return v
    }()

    var audioStatus: ContactAudioStatusView = {
        let v = ContactAudioStatusView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.cornerRadius = 0
        
        return v
    }()

    var noStatus: ContactNoStatusView = {
        let v = ContactNoStatusView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        //v.backgroundColor = UIColor(0xE3F2FD).withAlphaComponent(0.5)
        v.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)

        v.layer.cornerRadius = 8

        return v
    }()
    
    var statusContainerView: UIView  = {
        let v = UIView(frame: .zero)
        v.translatesAutoresizingMaskIntoConstraints = false
        
        return v
    }()

    var vipButton: UIButton = {

        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setImage(UIImage(imageLiteralResourceName: "addFavorite").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.setImage(UIImage(imageLiteralResourceName: "favorite_filled").withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .selected)
        button.tintColor = .black
        
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        button.layer.cornerRadius = 15
        
        button.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        return button
        
    }()

    var add_deleteButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Add", for: .normal)
        button.setImage(nil, for: .normal)
        button.setImage(UIImage(named: "more")?.withRenderingMode(.alwaysTemplate), for: .selected)
        button.setTitle(nil, for: .selected)
        button.tintColor = .black
        
        button.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        button.titleEdgeInsets = UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        button.heightAnchor.constraint(equalToConstant: 30).isActive = true
        //button.widthAnchor.constraint(equalToConstant: 30).isActive = true
        button.layer.cornerRadius = 15
        
        button.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        return button
        
    }()

    var communicateButton: UIButton = {
        
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.tintColor = .black
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        button.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
        button.heightAnchor.constraint(equalToConstant: 40).isActive = true
        button.layer.cornerRadius = 8
        button.setTitle("Contact Me", for: .normal)
        button.setTitle("Sign In To Contact Me", for: .selected)
        
        button.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        return button
        
    }()
    

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.widthAnchor.constraint(equalToConstant: (UIApplication.shared.keyWindow?.rootViewController!.view.frame.size.width)!).isActive = true
        
        self.addSubview(self.avatar)
        self.addSubview(self.name)
        self.addSubview(self.desc)
        self.addSubview(self.communicateButton)
        self.addSubview(self.add_deleteButton)
        self.addSubview(self.statusContainerView)
        
        //self.avatar.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        self.avatar.topAnchor.constraint(equalTo: self.statusContainerView.bottomAnchor, constant: 10).isActive = true
        self.avatar.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 15).isActive = true
        
        self.name.centerYAnchor.constraint(equalTo: self.avatar.centerYAnchor, constant: 0).isActive = true
        self.name.leftAnchor.constraint(equalTo: self.avatar.rightAnchor, constant: 10).isActive = true
        self.name.rightAnchor.constraint(equalTo: self.add_deleteButton.leftAnchor, constant: -15).isActive = true
        
        self.desc.topAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 8).isActive = true
        self.desc.leftAnchor.constraint(equalTo: self.avatar.leftAnchor, constant: 0).isActive = true
        self.desc.rightAnchor.constraint(equalTo: self.name.rightAnchor, constant: 0).isActive = true
        //self.desc.bottomAnchor.constraint(equalTo: self.avatar.bottomAnchor, constant: 0).isActive = true

        //self.statusContainerView.topAnchor.constraint(equalTo: self.desc.bottomAnchor, constant: 10).isActive = true
        self.statusContainerView.topAnchor.constraint(equalTo: self.topAnchor, constant: 15).isActive = true
        self.statusContainerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.statusContainerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true

        self.add_deleteButton.centerYAnchor.constraint(equalTo: self.avatar.centerYAnchor, constant: 0).isActive = true
        self.add_deleteButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        //self.communicateButton.topAnchor.constraint(equalTo: self.statusContainerView.bottomAnchor, constant: 15).isActive = true
        self.communicateButton.topAnchor.constraint(equalTo: self.desc.bottomAnchor, constant: 15).isActive = true
        
        self.communicateButton.leftAnchor.constraint(equalTo: self.avatar.leftAnchor, constant: 0).isActive = true
        self.communicateButton.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -15).isActive = true
        
        self.communicateButton.backgroundColor = self.tintColor
        self.communicateButton.setTitleColor(.white, for: .normal)
        self.add_deleteButton.backgroundColor = self.tintColor
        self.add_deleteButton.tintColor = .white
        
        
        self.bottomAnchor.constraint(equalTo: self.communicateButton.bottomAnchor, constant: 15).isActive = true

        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var shadowed: Bool! = true
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if(shadowed || self.contact.statusContent == nil) { return }
        
        print("Create shadow")
        let shadowView = UIView(frame: CGRect(x: 20,
                                              y: 20,
                                              width: self.statusContainerView.bounds.width - (2 * 20),
                                              height: self.statusContainerView.bounds.height - (2 * 20)))
        self.statusContainerView.insertSubview(shadowView, at: 0)

        let shadowPath = UIBezierPath(roundedRect: shadowView.bounds, cornerRadius: 14.0)
        shadowView.layer.masksToBounds = false
        shadowView.layer.shadowRadius = 8.0
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 20)
        shadowView.layer.shadowOpacity = 0.35
        shadowView.layer.shadowPath = shadowPath.cgPath
        
        self.shadowed = true
        
        //shadowView.setShadowWithColor(color: UIColor.red, opacity: 0.9, offset: CGSize(width: 3, height: 3), radius: 8, viewCornerRadius: 10)

    }
    
    
}

public class CKContactDetailsViewController: UIViewController {
    
    var _contact: CKContact!
    
    var contact: CKContact! {
        didSet {
            self.header.contact = contact
            self.loadData()
        }
    }
    var config: CKContactDetailsViewConfig! {
        
        didSet {
            self.view.backgroundColor = config.backgroundColor
        }
        
    }
    
    var header: ContactHeaderView! = {

        //let view = ContactHeaderView(frame: CGRect(x: 0, y: 0, width: 300, height: 250))
        let view = ContactHeaderView(frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view

    }()

    var tableView: UITableView! = {

        let t = UITableView(frame: .zero, style: .plain)
        
        return t
        
    }()
    
    var datasource: FUIFirestoreTableViewDataSource!
    
    var presentedVC: AnyClass!
    
    override public func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        
        self.navigationController?.navigationBar.prefersLargeTitles = false
        
        self.contact = _contact
        
        self.view.addSubview(self.tableView)
        self.tableView.frame = self.view.frame
        
        self.tableView.tableHeaderView = self.header
        self.tableView.tableHeaderView!.layoutIfNeeded()
        self.tableView.tableHeaderView = self.tableView.tableHeaderView
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 80, right: 0)
        
       
        // Do any additional setup after loading the view.
        
        self.checkIfContactIsInUsersContactList()
        
        self.header.vipButton.addTarget(self, action: #selector(self.vipAction), for: .touchDown)
        self.header.communicateButton.addTarget(self, action: #selector(self.communicateAction), for: .touchDown)
        
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.delegate = self
        self.tableView.sectionHeaderHeight = 80
        //self.tableView.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        
        self.header.videoStatus.playButton.addTarget(self, action: #selector(self.showStatus), for: .touchDown)
        self.header.audioStatus.playButton.addTarget(self, action: #selector(self.showStatus), for: .touchDown)

        let gesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTapAction))
        self.view.addGestureRecognizer(gesture)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.userStateChange), name: .userSignedOut, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.userStateChange), name: .userSignedIn, object: nil)


    }
    
    open func addNavigationItem() {
        let closeItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.closeAction))
        self.navigationItem.rightBarButtonItem = closeItem
    }
    
    @objc func userStateChange() {
        
        self.loadData()
        
    }
    
    @objc func longTapAction() {

        if(ConversationKit.shared.clientKey == "communicator") {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.communicate.simulate.action"), object: nil, userInfo: ["contact" : self.contact, "fromView" : self.header.communicateButton])
        }
        
    }
    
    private func loadData() {
        
        if(ConversationKit.shared.currentUser.isAnonymous) {
            
            let notSignedInView = AnonmyousView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 200))
            self.tableView.tableFooterView = notSignedInView
            self.header.communicateButton.isSelected = true
            
        }
        else {

            let noDataView = NoConversationView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
            noDataView.contact = self.contact
            self.tableView.tableFooterView = noDataView
            self.header.communicateButton.isSelected = false

            
            self.tableView.register(TopicOnlyCell.self, forCellReuseIdentifier: TopicOnlyCell.reuseId)
            self.tableView.register(TopicActionCell.self, forCellReuseIdentifier: TopicActionCell.reuseId)

            self.tableView.register(CKContactConversationCell.self, forCellReuseIdentifier: CKContactConversationCell.reuseId)
            self.tableView.register(CKContactConversationFirstMessageInCell.self, forCellReuseIdentifier: CKContactConversationFirstMessageInCell.reuseId)
            self.tableView.register(CKContactConversationFirstMessageOutCell.self, forCellReuseIdentifier: CKContactConversationFirstMessageOutCell.reuseId)


            let query = ConversationKit.shared.db.collection(ConversationKit.shared.appUser.id).document("data").collection("conversations").whereField("contact", isEqualTo: self.contact.id).order(by: "lastMessage", descending: true)
            
            self.datasource = self.tableView.bind(toFirestoreQuery: query, populateCell: { (tableView, indexPath, snapshot) -> UITableViewCell in

                print("LoadData bind")
                self.tableView.tableFooterView = nil
                
                
                let data = snapshot.data()
                let id = snapshot.documentID
                
                if(data != nil) {

                    let conversation = CKConversation(id: snapshot.documentID, dictionary: data!)
                    if(conversation?.intentMessageContent != nil) {
                        if(conversation?.lastMessageContent != nil) {

                            let cell = self.tableView.dequeueReusableCell(withIdentifier: CKContactConversationCell.reuseId, for: indexPath) as! CKContactConversationCell
                            cell.selectionStyle = .none
                            cell.conversation = conversation
                            
                            cell.playMessageAction = {message in
                                
                                self.playMessageAction(message: message)
                                
                            }
                            
                            
                            return cell
                            
                        }
                        else {
                            let cell = self.tableView.dequeueReusableCell(withIdentifier: CKContactConversationFirstMessageOutCell.reuseId, for: indexPath) as! CKContactConversationFirstMessageOutCell
                            cell.selectionStyle = .none
                            cell.conversation = conversation
                            
                            
                            cell.playMessageAction = {message in
                                
                                self.playMessageAction(message: message)
                                
                            }
                            
                            
                            return cell
                        }
                    }

                }

                return UITableViewCell()
                
                
            })
        }
        
    
    }
    
    private func playMessageAction(message: CKMessage) {
        
        if(message.messageType! == .voice) {
            let vc = VoiceMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder

            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
            
        }
        else if(message.messageType! == .video) {
            let vc = VideoMessagePlayerViewController()
            vc.message = message
            vc.transitioningDelegate = self
            vc.modalPresentationStyle = .custom
            self.presentedVC = vc.classForCoder
            
            
            self.present(vc, animated: true) {
                vc.playPauseAction()
            }
        }
        
        
        
    }

    private func checkIfContactIsInUsersContactList() {
        
        if(ConversationKit.shared.appUser != nil) {
            
            ConversationKit.shared.db.collection(ConversationKit.shared.currentUser.uid).document("data").collection("contacts").document(self.contact.id).getDocument { (documentSnapshot, error) in
                
                guard let document = documentSnapshot else {
                    print(error?.localizedDescription ?? "Error reading contact from personal contact list")
                    self.header.add_deleteButton.isSelected = false
                    return
                }
                
                guard let data = document.data() else {
                    self.header.add_deleteButton.isSelected = true
                    return
                }
                
                let c = CKContact(id: document.documentID, dictionary: data)
                self.header.add_deleteButton.isSelected = false

                
                
                
            }
            
        }
        else {
            self.header.vipButton.isSelected = false
        }
        
    }
    
    @objc public func closeAction() {
        
        self.dismiss(animated: true) {
        }
    }
    
    override public func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if(datasource != nil) {
            self.datasource.unbind()
        }
        super.dismiss(animated: flag, completion: completion)
    }
    
    @objc private func vipAction() {

        if(ConversationKit.shared.appUser == nil) {
            
        }
        else {
            
            let title = self.header.vipButton.isSelected ? "Remove Contact" : "Add Contact"
            let message = self.header.vipButton.isSelected ? "Do you want to remove the contact from your personal contact list ?" : "Do you want to add the contact to your personal contact list"
            
            let actionVC = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)

            let action1 = UIAlertAction(title: "Remove Contact", style: .destructive) { (action) in
            }

            let action2 = UIAlertAction(title: "Add Contact", style: .default) { (action) in
                CKContact.addToMyContactList(contact: self.contact!, completion: { (success) in
                    if(success) {
                    }
                })
            }
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            }

            if(self.header.vipButton.isSelected) {
                actionVC.addAction(action1)
            }
            else {
                actionVC.addAction(action2)
            }
            actionVC.addAction(cancel)
            
            self.present(actionVC, animated: true) {
            }

        }
        
        
        
    }
    
    @objc private func communicateAction() {
        
        if(ConversationKit.shared.currentUser.isAnonymous) {

            ConversationKit.requestAuthenticationToken()

        }
        else {

            if(ConversationKit.shared.clientKey == "communicator") {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "communicator.communicate.action"), object: nil, userInfo: ["contact" : self.contact, "fromView" : self.header.communicateButton])
            }
            else {
                
            }
            
        }
        
    }
    
    @objc private func showStatus() {
    
    
        let vc = ContactStatusMessageViewController()
        vc.contact = self.contact
        
        vc.transitioningDelegate = self
        vc.modalPresentationStyle = .custom
        self.presentedVC = vc.classForCoder

        self.present(vc, animated: true) {
            vc.play(completion: {
                vc.dismiss(animated: true, completion: {
                })
            })
        }
        
        /*
        self.navigationController?.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        */
        
    }
    
    
    public init(contact: CKContact) {
        super.init(nibName: nil, bundle: nil)
        self._contact = contact
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        
        print("CKContactDetailsViewController - deInit")
    }
    
    public static func open(contact: CKContact, modal: Bool) {
        
        let vc = CKContactDetailsViewController(contact: contact)
        vc.title = "Details"
        
        print(UIApplication.shared.keyWindow?.rootViewController?.description)

        
        if(modal == true) {
            vc.addNavigationItem()
            UIApplication.shared.keyWindow?.rootViewController?.present(UINavigationController(rootViewController: vc), animated: true, completion: {
            })
        }
        else {
            if UIApplication.shared.keyWindow?.rootViewController! is UITabBarController {
                print("1")
                let tabVC = UIApplication.shared.keyWindow?.rootViewController! as! UITabBarController
                let selectedVC = tabVC.selectedViewController
                if(selectedVC is UINavigationController) {
                    print("2")
                    
                    (selectedVC! as! UINavigationController).pushViewController(vc, animated: true)
                }
            }
            else if(UIApplication.shared.keyWindow?.rootViewController?.navigationController != nil) {
                print("3")
                UIApplication.shared.keyWindow?.rootViewController?.navigationController?.pushViewController(vc, animated: true)
            }
        }

        
        
    }
    

}

extension CKContactDetailsViewController: UINavigationControllerDelegate {
    
    public func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        if(viewController.classForCoder == ContactStatusMessageViewController.self) {
            
            (viewController as! ContactStatusMessageViewController).play {
                viewController.navigationController?.popViewController(animated: true)
            }
            
        }
    }
    
}

// MARK:- BonsaiController Delegate
extension CKContactDetailsViewController: BonsaiControllerDelegate {
    
    public func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        
        if(presented.classForCoder == VoiceMessagePlayerViewController.classForCoder() || presented.classForCoder == VideoMessagePlayerViewController.classForCoder()) {
            return BonsaiController(fromDirection: .bottom, blurEffectStyle: .dark, presentedViewController: presented, delegate: self)
        }
        else {
            return BonsaiController(fromView: self.header.statusContainerView, blurEffectStyle: .dark,  presentedViewController: presented, delegate: self)
        }
    }
    
    public func frameOfPresentedView(in containerViewFrame: CGRect) -> CGRect {
        
        if(self.presentedVC == VoiceMessagePlayerViewController.classForCoder() || self.presentedVC == VideoMessagePlayerViewController.classForCoder()) {
            return CGRect(origin: CGPoint(x: 0, y: containerViewFrame.size.height * 0.3), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height * 0.7))
        }
        else {
            return CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: containerViewFrame.width, height: containerViewFrame.height))
        }

    }
}

extension CKContactDetailsViewController {

    func playMessage(message: CKMessage) {
        
        switch message.messageType! {
        case .video:
            self.playVideoMessage(message: message)
        case .voice:
            self.playVoiceMessage(message: message)
        default:
            print("Unsupported Message Type")
        }
        
    }
    
    func playVideoMessage(message: CKMessage) {

        
    }
    
    func playVoiceMessage(message: CKMessage) {
        
    }
    
}

extension CKContactDetailsViewController: UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 100))

        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        label.text = "Conversations"
        view.addSubview(label)
        label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 15).isActive = true
        label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -15).isActive = true
        label.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -5).isActive = true
        view.backgroundColor = self.view.backgroundColor
        
        return view
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! CKContactConversationBaseCell
        
    }
    
    
}


