//
//  CKIntentView.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 29.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import UIKit
import SwiftRichString

public class DashedLineView: UIView {
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.makeDashedBorderLine()
    }
    
}

public class CKIntentView: UIView {

    public var intent: CKIntent! {
        didSet {
            
            self.backgroundColor = intent.getColor().backgroundColor
            self.intentName.textColor = intent.getColor().tintColor
            
            self.intentName.text = intent.name
            
        }
    }
    
    var intentName: UILabel = {
        
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)

        label.numberOfLines = 0
        
        return label
    }()

    
    
    private func configure() {
        self.addSubview(self.intentName)
        self.intentName.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        self.intentName.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        self.intentName.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
        self.intentName.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true

        
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}

public class CKIntentActionView: UIView {
    
    var message: CKMessage! {

        didSet {
            
        }

    }
    
    var fulfillmentSummary: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        
        label.textColor = .darkGray
        label.textAlignment = .left
        label.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        label.numberOfLines = 0
        
        return label
    }()
    
    public func style(styleGroup: StyleGroup) {

        let text = "<action>  \(self.message.topicActionDetails.name)  </action> \(self.message.text!)"

        self.fulfillmentSummary.attributedText = text.set(style: styleGroup)
        
    }

    
    private func configure() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(self.fulfillmentSummary)
        self.fulfillmentSummary.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        self.fulfillmentSummary.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 0).isActive = true
        self.fulfillmentSummary.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 0).isActive = true
        self.fulfillmentSummary.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
    }

    
    
}
