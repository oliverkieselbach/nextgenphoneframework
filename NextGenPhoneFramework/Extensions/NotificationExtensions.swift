//
//  NotificationExtensions.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 26.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let userSignedIn = Notification.Name("userSignedIn")
    static let userSignedOut = Notification.Name("userSignedOut")
}
