//
//  UIViewExtensions.swift
//  NextGenPhoneApp
//
//  Created by Oliver Kieselbach on 05.10.18.
//  Copyright © 2018 Kieselbach, Oliver. All rights reserved.
//

import Foundation
import UIKit


enum GradientDirection {
    case leftToRight
    case rightToLeft
    case topToBottom
    case bottomToTop
    case topLeftToBottomRight
}

extension UIButton {
    func setBackgroundColor(_ color: UIColor, forState controlState: UIControl.State) {
        let colorImage = UIGraphicsImageRenderer(size: CGSize(width: 1, height: 1)).image { _ in
            color.setFill()
            UIBezierPath(rect: CGRect(x: 0, y: 0, width: 1, height: 1)).fill()
        }
        setBackgroundImage(colorImage, for: controlState)
    }
}

extension UIView {
    func setShadowWithColor(color: UIColor?, opacity: Float?, offset: CGSize?, radius: CGFloat, viewCornerRadius: CGFloat?) {
        //layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: viewCornerRadius ?? 0.0).CGPath
        layer.shadowColor = color?.cgColor ?? UIColor.black.cgColor
        layer.shadowOpacity = opacity ?? 1.0
        layer.shadowOffset = offset ?? CGSize.zero
        layer.shadowRadius = radius ?? 0
    }
    /*
    func addBlurView() {
        let mask = autoresizingMask
        let blurContainer = UIView(frame: frame)
        addSubview(blurContainer)
        blurContainer.frame = bounds
        blurContainer.autoresizingMask = mask
        let effect = UIBlurEffect(style: .extraLight)
        let blurView = UIVisualEffectView(effect: effect)
        blurContainer.addSubview(blurView)
        blurView.frame = bounds
        blurView.autoresizingMask = mask
    }
    */
    
    func addBlurView(style: UIBlurEffect.Style? = .extraLight) {

        for view in self.subviews {
            if(view.tag == 9999) {
                view.removeFromSuperview()
                break
            }
        }

        let mask = autoresizingMask
        let blurContainer = UIView(frame: frame)
        blurContainer.tag = 9999
        addSubview(blurContainer)
        blurContainer.frame = bounds
        blurContainer.autoresizingMask = mask
        let effect = UIBlurEffect(style: style!)
        let blurView = UIVisualEffectView(effect: effect)
        blurContainer.addSubview(blurView)
        blurView.frame = bounds
        blurView.autoresizingMask = mask
    }
    
    func gradientBackground(from color1: UIColor, to color2: UIColor, direction: GradientDirection) {
        
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color1.cgColor, color2.cgColor]
        gradient.locations = [0.0, 0.5, 0.95]
        switch direction {
        case .leftToRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
        case .rightToLeft:
            gradient.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 0.0, y: 0.5)
        case .bottomToTop:
            gradient.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 0.0)
        case .topToBottom:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.0, y: 1.0)
        case .topLeftToBottomRight:
            gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
            gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        }

        
        let layer = self.layer.sublayers == nil ? CALayer() : self.layer.sublayers![0]
        if(layer.classForCoder == CAGradientLayer.classForCoder()) {
            self.layer.replaceSublayer(layer, with: gradient)
        }
        else {
            self.layer.insertSublayer(gradient, at: 0)
        }
        
        
        
    }
    
    private static let lineDashPattern: [NSNumber] = [3, 3]
    private static let lineDashWidth: CGFloat = 1.5
    
    func makeDashedBorderLine() {
        let path = CGMutablePath()
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = UIView.lineDashWidth
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineDashPattern = UIView.lineDashPattern
        path.addLines(between: [CGPoint(x: bounds.minX, y: 0),
                                CGPoint(x: bounds.minX, y: bounds.height)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }

    public func makeHorizontalDashedBorderLine() {
        let path = CGMutablePath()
        let shapeLayer = CAShapeLayer()
        shapeLayer.lineWidth = UIView.lineDashWidth
        shapeLayer.strokeColor = UIColor.lightGray.cgColor
        shapeLayer.lineDashPattern = UIView.lineDashPattern
        path.addLines(between: [CGPoint(x: bounds.minX, y: 0),
                                CGPoint(x: bounds.maxX, y: 0)])
        shapeLayer.path = path
        layer.addSublayer(shapeLayer)
    }

}
