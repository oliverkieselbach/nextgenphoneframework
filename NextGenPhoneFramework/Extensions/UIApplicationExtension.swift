//
//  UIApplicationExtension.swift
//  NextGenPhoneFramework
//
//  Created by Kieselbach, Oliver on 26.04.19.
//  Copyright © 2019 Kieselbach, Oliver. All rights reserved.
//

import Foundation

extension UIApplication {
    class func getTopMostViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return getTopMostViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return getTopMostViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return getTopMostViewController(base: presented)
        }
        return base
    }
}
